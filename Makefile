# this file is a part of Avaritia, made by John Nunley and licensed under GPL 3

# this file, when run using the "make" command, will build the program

# define the name of the build target
appname = avaritia

# compiler and flags
CC = gcc
CXX = g++
LIBFLAGS = -lglfw -lGL -lGLU -lGLEW -lpng -ljpeg
CFLAGS = -std=c11 -Iinclude -Wall -g
LDFLAGS = -g

# 1 if we are building asset manager
ASSETMANAGER = 1
ifeq ($(ASSETMANAGER), 1)
  CFLAGS += -DASSETMANAGER
endif

# names of source files, add as we go along
SRCDIR = src/
SRCS = $(SRCDIR)main.c \
       $(SRCDIR)am_datachunks.c \
       $(SRCDIR)am_dfops.c \
       $(SRCDIR)am_main.c \
       $(SRCDIR)am_interactive.c \
       $(SRCDIR)e_entity.c \
       $(SRCDIR)f_datafile.c \
       $(SRCDIR)f_image.c \
       $(SRCDIR)f_imageconv.c \
       $(SRCDIR)g_tick.c \
       $(SRCDIR)gl_draw.c \
       $(SRCDIR)gl_loop.c \
       $(SRCDIR)gl_renderer.c \
       $(SRCDIR)gl_screen.c \
       $(SRCDIR)gl_shaders.c \
       $(SRCDIR)i_args.c \
       $(SRCDIR)i_byte.c \
       $(SRCDIR)i_err.c \
       $(SRCDIR)i_file.c \
       $(SRCDIR)i_random.c \
       $(SRCDIR)i_salloc.c \
       $(SRCDIR)i_string.c \
       $(SRCDIR)im_animation.c \
       $(SRCDIR)im_text.c \
       $(SRCDIR)m_map.c 

# names of shader objects
SHADERDIR = src/shaders/
SHADERS = $(SHADERDIR)vertex.glsl \
	  $(SHADERDIR)fragment.glsl

# directory for objects
OBJDIR = objects/

# names of object files, axe the SRCDIR part and replace .c w/ .o
OBJS = $(subst $(SRCDIR),$(OBJDIR),$(patsubst %.c, %.o, $(SRCS)))

# target, run automatically in make command
all: $(appname)

# build executable
$(appname): $(OBJS) $(SHADERS)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $(appname) $(OBJS) $(LDLIBS) $(LIBFLAGS)

# clean up the directory
clean:
	$(RM) $(OBJS)

distclean: clean
	$(RM) $(appname)
	$(RM) *~

# general rule for compiling object files

$(OBJDIR)%.o: $(SRCDIR)%.c
	$(CC) $(CFLAGS) -c $< -o $@
