// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// this file contains some useful utilities for opening, reading, and saving files

#ifndef I_FILE_H
#define I_FILE_H

#include <stdio.h>

#include "i_types.h"

// read [count] amount of bytes from [stream] into [buffer]
short readbytes(byte *buffer, FILE *stream, short count);

// read a string into [buffer] until a newline (\n) is encountered
void readstring(char *buffer, FILE *stream, short *length);

// write a set number of bytes into a file
void writebytes(byte *buffer, FILE *stream, short count);

// write a string to a file
void writestring(char *buffer, FILE *stream);

// checked fwrite
void checkfwrite(const void *buffer, size_t size, size_t len, FILE *stream);

void checkfread(void *buffer, size_t size, size_t len, FILE *stream);

// check if a file exists
bool fileexists(const char *filename);

// delete a file
void checkremove(const char *filename);

// a version of the fgets function w/ error checking
void checkfgets(char *buffer, short maxbytes, FILE *stream);

// fseek w/ error checking
void checkfseek(FILE *stream, long offset, int origin);

// get the size of a file
long getfilesize(FILE *stream);

// tell if we are at the end of a file
bool atendoffile(FILE *stream);

// read the entire contents of a file into a string
char *readentirefile(const char *filename);

// copy a file from one location to another
void copyfile(const char *filename1, const char *filename2);

unsigned long checkftell(FILE *stream);


// basic thing to write to a byte buffer
#define SET_BUFFER_BYTES(buf,b1,b2,b3,b4,b5) buf[0] = b1; \
					     buf[1] = b2; \
                                             buf[2] = b3; \
                                             buf[3] = b4; \
                                             buf[4] = b5


#endif
