// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// file used to generate random numbers

#ifndef I_RANDOM_H
#define I_RANDOM_H

unsigned short genrandom(unsigned short min, unsigned short max);

#endif
