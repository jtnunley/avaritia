// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// program's "emergency exit". the function here is to be used to get out of the program if there's an emergency and we need to exit
//
// NOTE: if we're working with pointers, make sure to free memory first. probably not important (most modern kernels should just auto-free the memory when the program goes belly up) but if we ever decide to port to anything older it might cause some issues (might also cause issues on modern os, you never know :-)

#ifndef I_ERR_H
#define I_ERR_H

#include "i_types.h"

void error(const char *msg, bool sys);

#endif
