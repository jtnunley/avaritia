// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// an entity, i.e. an npc with some stats

#ifndef E_ENTITY_H
#define E_ENTITY_H

#include "f_datafile.h"
#include "i_types.h"
#include "im_animation.h"

#include <stdio.h>
#include <time.h>

#define CURRENT_ENT_VERSION 1

typedef enum {
  left = 0,
  right = 1,
  up = 2,
  down = 3
} direction;

// entity
typedef struct entity {
  char *name;
  bool err;

  byte depCount;
  char **dependencies;

  // walking/standing animations
  animation *walking; 
  animation *standing;

  byte walkingproperties;
  // 0 = stand still
  // 1 = look at player
  // 2 = random wandering
  // 3 = follow player at distance
  // 4 = attempt to interact with player
  // 5 = random walk within radius
  // 6 = follow path

  // data for walking
  union {
    byte followdistance;
    byte walkradius;
    struct {
      direction *path;
      unsigned short pathlen;
    } pathdata;
  } walkingdata;

  unsigned short walktimer;

  // faction information
  byte faction;
  // 0 = hostile no matter what
  // 1 = friendly no matter what
  // 2 = neutral
  // other # = external faction
  
  // x and y pos (in qtiles)
  unsigned short x, y;

  // width and height of entity hitbox (in qtiles)
  byte width, height;

  // health points (all entities can be damaged)
  byte health;

  // byte to store boolean about combat
  byte combatproperties;
  // 0 = has been attacked
  // 1 = is dead

  // starting timer for time related things
  time_t startingtime;

  // TODO: weapon and armor

  // TODO: dialogue
} entity;

entity *loadent(FILE *stream, datafile *df, unsigned short x, unsigned short y);
void saveent(FILE *stream, entity *ent);
void deleteent(entity *ent);

bool damageent(entity *ent, byte damage);
void runtick(entity *ent);

#endif
