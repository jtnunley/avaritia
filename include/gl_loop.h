// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

#ifndef GL_LOOP_H
#define GL_LOOP_H

int beginGLLoop();

#endif
