// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// this file contains a function to malloc data and then check to ensure that we're not out of memory
//
// NOTE: same precautions for malloc apply here as well. free your memory.

#ifndef I_SALLOC_H
#define I_SALLOC_H

#include <stddef.h>

void *checkalloc(size_t size);
void checkfree(void *ptr);

#endif
