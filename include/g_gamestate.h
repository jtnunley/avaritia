// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// a struct that binds together many states of the game

#ifndef G_GAMESTATE_H
#define G_GAMESTATE_H

#include "f_datafile.h"
#include "m_map.h"

typedef struct gamestate {
  char *filename;
  short phase;

  datafile *gamefile;
  map_t *currentmap;
  image_t *mapdata;
} gamestate;

#endif
