// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// defines animations, e.g. sequences of images

#ifndef IM_ANIMATION_H
#define IM_ANIMATION_H

#include "f_datafile.h"
#include "f_image.h"
#include "i_types.h"

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define CURRENT_ANIM_VERSION 1

typedef struct animation {
  char *name;
  bool err;

  byte depCount;
  char **dependencies;

  // pictures used for animation
  image_t **frames;
  byte currentframe;

  // frame switch ctr
  unsigned short switchcntr;
  
  // internal time counter
  time_t starttime;
} animation;

// management of animations
animation *loadanim(FILE *stream, datafile *df);
void saveanim(animation *anim, FILE *stream);
void deleteanim(animation *anim);

// run a tick on the animation
void animtick(animation *anim);

// get the current frame of the animation
image_t *getframe(animation *anim);

// manually advance the animation
void advanceframe(animation *anim);

#endif
