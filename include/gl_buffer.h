// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// stores information about the current buffer

#ifndef GL_BUFFER_H
#define GL_BUFFER_H

typedef struct glbuffer {
  unsigned int buffer; 
  // unsigned int globcount;
} glbuffer;

#endif
