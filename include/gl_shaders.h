// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// code responsible for loading and compiling shaders

#ifndef GL_SHADERS_H
#define GL_SHADERS_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

GLuint loadshaders(const char *vertexfile, const char *fragmentfile);

#endif
