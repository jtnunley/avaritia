// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// contains a function for drawing on a window

#ifndef GL_DRAW_H
#define GL_DRAW_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <stdlib.h>

#include "g_types.h"
#include "gl_buffer.h"

GLuint initvertexdiagram();
GLuint initcolordiagram();
GLuint inittexturediagram();
GLuint *initindexdiagram();
GLuint createcolortexture();

void initdraw();
void drawscreen(GLFWwindow *window, GLuint program, GLuint vertexbuffer, GLuint colorbuffer, GLuint texturebuffer, GLuint texture, GLuint *indicies);
void changecolor(GLuint texture, unsigned short x, unsigned short y, color_t clr);

#endif
