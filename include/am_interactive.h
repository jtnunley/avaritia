// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// contains several tools for the AssetManager program to interact with the user, like menus, yesnos, etc.

#ifndef AM_INTERACTIVE_H
#define AM_INTERACTIVE_H

#include "i_types.h"

bool yesno(const char *prompt);
char createmenu(char options[], char *descriptions[], byte optNum);

byte getnumber(const char *prompt);

void readcolor(byte *r, byte *g, byte *b);

#endif
