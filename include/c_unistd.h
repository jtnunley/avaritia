// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// this file will just import unistd.h, or any equivalent functions on windows

#ifndef C_UNISTD_H
#define C_UNISTD_H

#include "c_oscheck.h"

// if we are using unix, just include unistd.h
#ifdef C_OS_UNIX
#include <unistd.h>
// #include <string.h> // strsep();
#elif

// some headers that contains unistd.h functions
#include <stdlib.h>
#include <io.h>
#include <process.h>
#include <direct.h>

#define srandom srand
#define random rand

// values for access() function
#define R_OK 4
#define W_OK 2
#define F_OK 0

// changing the names around of some important functions
#define access _access
#define dup2 _dup2
#define execve _execve
#define ftruncate _chsize
#define unlink _unlink
#define fileno _fileno
#define getcwd _getcwd
#define chdir _chdir
#define isatty _isatty
#define lseek _lseek

// define ssize_t
#ifdef _WIN64
#define ssize_t __int64
#else
#define ssize_t long
#endif

// std file denominators
#define STDIN_FILENO  0
#define STDOUT_FILENO 1
#define STDERR_FILENO 2

// strsep
// char *strsep(char **stringptr, const char *delim);

#endif

#endif
