// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// this file handles the rendering of text into images

#ifndef IM_TEXT_H
#define IM_TEXT_H

#include "f_datafile.h"
#include "f_image.h"
#include "gl_buffer.h"

extern datafile *textstorage;
void inittextrender(const char *textfile);
void rendertxt(glbuffer *buffer, const char *txt, unsigned short x, unsigned short y, color_t clr);

#endif
