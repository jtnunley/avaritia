// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// this file is responsible for function interfacing between external image libraries (libpng and libjpg) and Avaritia's internal image system

#ifndef F_IMAGECONV
#define F_IMAGECONV

#include "f_image.h"

image_t *loadimgexternal(const char *filename);

#endif
