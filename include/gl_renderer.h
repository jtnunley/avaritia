// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// this file handles the rendering of LIT types like images onto OpenGL

#ifndef GL_RENDERER_H
#define GL_RENDERER_H

#include "f_image.h"
#include "gl_screen.h"
#include "m_map.h"

void drawimage(glbuffer *buffer, image_t *img, unsigned short x, unsigned short y);
void drawimagep(glbuffer *buffer, image_t *img, unsigned short x, unsigned short y, color_t *pallete);
void drawimages(glbuffer *buffer, image_t *img, unsigned short x, unsigned short y, unsigned short pixXScale, unsigned short pixYScale);
void drawimageps(glbuffer *buffer, image_t *img, unsigned short x, unsigned short y, color_t *pallete, unsigned short pixXScale, unsigned short pixYScale);

void drawmap(glbuffer *buffer, map_t *map, unsigned short cornerX, unsigned short cornerY);

#endif
