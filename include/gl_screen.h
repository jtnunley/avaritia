// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// contains variable for the screen of globules, as well as some other variables

#ifndef GL_SCREEN_H
#define GL_SCREEN_H

// one tile is made up of 10x10 globs
#define GLOBS_PER_TILE_WIDTH 10
#define GLOBS_PER_TILE_HEIGHT 10

// screen is 16 tiles by 12 tiles
#define TILES_PER_SCREEN_WIDTH 16
#define TILES_PER_SCREEN_HEIGHT 12

// calculate globs on screen
// calculated as extern in order to save computation time
extern unsigned short globsperscreenW;
extern unsigned short globsperscreenH;

#define DEFAULT_SCREEN_WIDTH_PX 720
#define DEFAULT_SCREEN_HEIGHT_PX 720

#include "g_types.h"
#include "gl_buffer.h"

void modifyglob(glbuffer *buffer, unsigned short x, unsigned short y, color_t clr);
void clearscreen(glbuffer *buffer, color_t clr);

#endif
