// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// this file contains the main loop for Avaritia's internal AssetManager program, which is in charge for creating and managing datafiles

#ifndef AM_MAIN_H
#define AM_MAIN_H

#ifdef ASSETMANAGER
void beginassetmanager();
#endif

#endif
