// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// this file handles interactions with the Datafiles for the interal AssetManager

#ifndef AM_DFOPS_H
#define AM_DFOPS_H

#include "f_datafile.h"
#include "f_image.h"
#include "m_map.h"

void summarizedatafile(datafile *df);

// image ops
void am_createimg(datafile *df);
void am_modifyimg(datafile *df, char *name);
void am_imagedata(image_t *img);

// map ops
void am_createmap(datafile *df);
void am_modifymap(datafile *df, char *name);

// testing
void am_displayimagedata(image_t *img);


#endif
