// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// defines functions for handling args, makes it easier to access flags et al

#ifndef I_ARGS_H
#define I_ARGS_H

extern int argc_;
extern char **argv_;

// this function searches for a given argument and returns its index (0 if not found)
int argcheck(const char *input);

#endif
