// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// global vars associated with the game as a whole

#ifndef GLOBALVARS_H
#define GLOBALVARS_H

extern unsigned long int gameticks;

#endif
