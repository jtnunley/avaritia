// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// this file contains functions to manipulate bytes

#ifndef I_BYTE_H
#define I_BYTE_H

#include "i_types.h"

byte upperhalf(byte b);
byte lowerhalf(byte b);
byte combinehalves(byte upper, byte lower);

// used for using bytes as boolean storage
bool evalByteAt(byte b, byte index);
byte setByteVal(byte b, byte index, bool value);

#endif
