// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// this file contains functions and data structures for maps

#ifndef M_MAP_H
#define M_MAP_H

#include <stdio.h>

#include "g_types.h"
#include "f_datafile.h"
#include "f_image.h"
#include "i_types.h"

#define CURRENT_MAP_VERSION 2

// a reasonable limit of tile types
#define MAX_TILETYPES 50

typedef struct tiletype {
    byte properties;
    // 0: exists
    // 1: is a wall tile 
    // 2: is a floor tile
    // 3: is an out of bounds tile
    // 4-7: blank (for now)
    color_t color;
} tiletype;

typedef struct map {
  byte width, height;

  /* Tile types:
    0 - should be a floor type
    1 - should be a wall type
    2 - should be an OOB type
    3-onwards - whatever is needed
  */
  tiletype *tiles;
  byte tilenumber;

  // tile map
  byte *pixeldata;

  // dependencies - usually just an image (for now)
  char **dependencies;
  byte dependencynumber;

  // error stuff
  bool err;
  char *name;

  byte version;

  // TODO: other map-related things, entities, map events, etc
} map_t;

map_t *loadmap(FILE *stream,datafile *df);
void savemap(FILE *stream, map_t *mp);
void deletemap(map_t *map);

#endif
