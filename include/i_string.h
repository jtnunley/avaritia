// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// this file contains utilities for manipulating strings

#ifndef I_STRING_H
#define I_STRING_H

#include "i_types.h"

// remove newline at end of string
void removenewline(char *str);

// check if a string is a number
bool isstrnumber(char *str);

// split a string by a delimiters
char **splitstring(char *str, const char delim, unsigned short *count);

// duplicate a string
char *strdup(const char *str);

#endif
