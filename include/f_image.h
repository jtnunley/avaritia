// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// this file contains headers and functions for loading and saving images

#ifndef F_IMAGE_H
#define F_IMAGE_H

#include "g_types.h"
#include "i_types.h"

#include <stdio.h>

#define CURRENT_IMAGE_VERSION 1

// struct to hold image data
typedef struct image {
  byte width,height;
  color_t *palletedata;
  byte palletelen;

  byte *pixeldata;

  char *name;

  bool err; // if an error occurred in the reading process, this is true

  byte version;
  // unsigned int len;
} image_t;

// load/save images in file streams
image_t *loadimg(FILE *stream);
void saveimg(image_t *img, FILE *stream);

// get a pixel from the image
color_t getpixel(image_t *img, byte x, byte y);

// remove image from memory
void deleteimg(image_t *img);

#endif
