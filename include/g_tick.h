// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// running a game tick

#ifndef G_TICK_H
#define G_TICK_H

#include "g_gamestate.h"
#include "g_globalvars.h"
#include "gl_buffer.h"
#include "i_types.h"

void initgame(gamestate *gt, const char *textfile);

// game tick will update glscreen with up to date game data
// TODO: pass in events somehow
bool gametick(gamestate *gt, glbuffer *buffer);

#endif
