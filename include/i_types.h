// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// this file defines several types that C does not define manually, like bool and byte

#ifndef I_TYPES_H
#define I_TYPES_H

// if we're compiling with C++, just use the built-in bool type, otherwise define our own
#ifndef __cplusplus
;
;
typedef enum {false=0,true=1} bool;
#endif

// byte is defined as unsigned char
typedef unsigned char byte;

// import float.h and limits.h, contains useful typing values
#include <limits.h>
#include <float.h>

#endif
