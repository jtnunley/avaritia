// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// this file contains data structures that are useful in a game sense

#ifndef G_TYPES_H
#define G_TYPES_H

#include "i_types.h"

// color
typedef struct {
  byte red,green,blue;
} color_t;

#define C_ALPHA_RED 0xA1
#define C_ALPHA_GREEN 0x94
#define C_ALPHA_BLUE 0xA0
#define IS_COLOR_ALPHA(clr) (clr.red == C_ALPHA_RED && clr.green == C_ALPHA_GREEN && clr.blue == C_ALPHA_BLUE)
#define IS_COLOR_EQUAL(clr1, clr2) (clr1.red == clr2.red && clr1.green == clr2.green && clr1.blue == clr2.blue)
#define SET_COLOR(clr1,clr2) clr1.red = clr2.red; clr1.green = clr2.green; clr1.blue = clr2.blue;

#endif
