// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// this file contains structs and headers for datafile management

#ifndef F_DATAFILE_H
#define F_DATAFILE_H

#include "i_types.h"

#include <stdio.h>

// types of data
#define DTYPE_IMAGE 48
#define DTYPE_MAP 49

// max num of loaded datachunks
#define MAX_DATACHUNKS 65300

// current datafile version
#define CURRENT_DFILE_VERSION 2

// declare some datatypes ahead of time
struct image;
struct map;
struct animation;

// datachunk - holds data
typedef struct datachunk {
  byte datatype;
  unsigned long pos;
  byte status; // 0 = error, 1 = unloaded, 2 = loaded
  
  char *name;

  char **dependencies;
  byte dependencynumber;

  union {
    struct image *img;
    struct map *mp;
    struct animation *anim;
  } data;
} datachunk;

// datafile - holds datachunks
typedef struct datafile {
  datachunk **datachunks;
  unsigned short chunknumber;

  char *filename;

  char *name;
  char *initialmap;

  byte version;
  unsigned long currentpos;
} datafile;

// chunk loading methods
void loadchunk(datachunk *chunk, datafile *file, FILE *stream);
void savechunk(datachunk *chunk, datafile *file, FILE *stream);
void unloadchunk(datachunk *chunk);

// loading chunks by name
datachunk *loadnamedchunk(datafile *file, const char *name, FILE *stream);
void savenamedchunk(datafile *file, const char *name, FILE *stream);
void unloadnamedchunk(datafile *file, const char *name);

// datafile loading methods
datafile *loaddatafile(const char *filename, char *err);
void savedatafile(datafile *df);

#endif
