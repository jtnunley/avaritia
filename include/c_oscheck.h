// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// this file simply checks if this program is running on windows or unix

#ifndef C_OSCHECK_H
#define C_OSCHECK_H

#if defined(_WIN32) || defined(WIN32)
#define C_OS_WINDOWS
#elif defined(__unix__) || defined(linux)
#define C_OS_UNIX
#endif

#endif
