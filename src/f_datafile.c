// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see f_datafile.h for more information

#include "f_datafile.h"
#include "f_image.h"
#include "i_err.h"
#include "i_file.h"
#include "i_salloc.h"
#include "i_string.h"
#include "m_map.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define DATAFILE_LOAD_ERROR(msg) { strcpy(err,msg); return NULL; }
#define PUSH_DATACHUNK { if (dc) { \
	                   df->datachunks[df->chunknumber] = dc; \
	                   df->chunknumber += 1; \
	                   dc = NULL; \
			 } }
	                   
#define DATACHUNK_LOAD_ERROR(msg) { dc->status = 0; \
	                            if (dc->name) checkfree(dc->name); \
	                            dc->name = strdup(msg); \
	                            PUSH_DATACHUNK; }
	                            

datafile *loaddatafile(const char *filename, char *err) {
    // before we begin, ensure that the file actually exists
    if (!(fileexists(filename))) DATAFILE_LOAD_ERROR("File does not exist");

    // open a stream
    FILE *stream = fopen(filename, "rb");
    if (!stream) DATAFILE_LOAD_ERROR("Unable to load file");
    
    // read magic bytes
    char buffer[256];
    checkfgets(buffer,sizeof(buffer),stream);
    if (strcmp(buffer,"LIT01") != 0) DATAFILE_LOAD_ERROR("File is not a valid datafile");

    // create the datafile
    datafile *df = checkalloc(sizeof(datafile));

    // set some default values
    df->filename = strdup(filename);
    df->datachunks = checkalloc(sizeof(datachunk *) * MAX_DATACHUNKS);
    df->chunknumber = 0;

    // read in name
    checkfgets(buffer, 256, stream);
    if (strlen(buffer) < 5) DATAFILE_LOAD_ERROR("File does not contain a valid datafile id");
    df->name = strdup(buffer);
    
    // read in version
    checkfread(&(df->version), 1, 1, stream);
    if (df->version > CURRENT_DFILE_VERSION) DATAFILE_LOAD_ERROR("File does not contain a valid version byte"); 

    // read to newline
    checkfgets(buffer, 256, stream); 

    // read initially loaded map
    checkfgets(buffer, 256, stream);
    printf("Read initial map: %s\n", buffer);
    if (strcmp(buffer, "(null)") == 0 || strlen(buffer) < 5) df->initialmap = NULL;
    else df->initialmap = strdup(buffer);

    // if we're at the end of the file, return
    if (atendoffile(stream)) return df;

    // puts("Beginning datafile loop");

    // go through the file and read datachunks
    int type;
    datachunk *dc;
    unsigned long pos;
    bool foundDependencies;
    byte bytes[50];
    while (fgets(buffer, 256, stream) != 0) {
      // determine if this is an LIT thing
      removenewline(buffer);
      if (strlen(buffer) != 5 ||
          buffer[0] != 'L' ||
	  buffer[1] != 'I' ||
	  buffer[2] != 'T') continue;

      // before anything else, get the position
      // we are at LIT##_\n_, so we need to o 6 back
      pos = checkftell(stream) - 6;
      // printf("Found datachunk at pos %ld\n",pos);

      // we've found a datachunk; check what type it is
      buffer[0] = buffer[3];
      buffer[1] = buffer[4];
      buffer[2] = '\0'; 
      type = atoi(buffer);
       
      // create datachunk in memory
      dc = checkalloc(sizeof(datachunk));
      dc->pos = pos;
      dc->status = 1;
      dc->datatype = (byte)type;
      if (dc->datatype > 100) DATACHUNK_LOAD_ERROR("Invalid datatype");

      // read in ID
      // prtf("Fgets call 2\n");
      checkfgets(buffer,256,stream);
      if (strlen(buffer) < 5) DATACHUNK_LOAD_ERROR("Datachunk has invalid ID");
      dc->name = strdup(buffer);

      // search for LIT03 (dependency signifier)
      pos = checkftell(stream);
      foundDependencies = false;
      while (fgets(buffer, 256, stream) != 0) {
	  removenewline(buffer);
	  if (strncmp(buffer, "LIT03", 5) != 0) {
            if (strncmp(buffer, "LIT", 3) == 0) break;
	    else continue;
	  } else {
            foundDependencies = true;
	    break;
	  }
      }

      if (!foundDependencies) { 
          dc->dependencynumber = 0; 
	  if (fseek(stream, pos, SEEK_SET) != 0) error("Unable to seek cursor in file", true);
      }
      else {         
          checkfread(bytes, 1, 1, stream);
	  
	  if (buffer[0] == 0) DATACHUNK_LOAD_ERROR("Invalid number of dependencies");

	  dc->dependencynumber = bytes[0];
	  dc->dependencies = checkalloc(sizeof(char *) * buffer[0]);
	  for (byte i = 0; i < dc->dependencynumber; i++) { 
	    checkfgets(buffer,256,stream); 
            dc->dependencies[i] = strdup(buffer);
	  }
      }

      PUSH_DATACHUNK;
    } 

    fclose(stream);

    return df;
}

// save a datafile to a file
void savedatafile(datafile *df) {
    datachunk *dc;
    bool loaded;

    // create a temporary duplicate file to load chunks from
    if (fileexists(df->filename)) copyfile(df->filename, "tmp.txt");

    // create filestreams
    FILE *stream = fopen(df->filename, "wb");
    FILE *backupfile;
    if (fileexists("tmp.txt")) backupfile = fopen("tmp.txt", "rb"); 

    // write magic bytes
    fputs("LIT01\n", stream);

    // write name
    fprintf(stream, "%s\n", df->name);

    // write version byte
    checkfwrite(&(df->version), 1, 1, stream);

    // write newline
    fputs("\n", stream);

    // write initially loaded map
    fprintf(stream, "%s\n", (df->initialmap ? df->initialmap : "(null)"));

    // puts("Beginning saving datachunks loop");

    // write datachunks
    for (unsigned int i = 0; i < df->chunknumber; i++) { 
      // if it isn't loaded, read it from the backup file
      dc = df->datachunks[i];

      // skip over the datachunk if there is an error
      if (dc->status == 0) continue;

      loaded = dc->status == 2;
      if (!loaded) loadchunk(dc, df, backupfile);

      // save the chunk
      savechunk(dc, df, stream);

      // if the chunk wasn't loaded before, unload it
      if (!loaded) unloadchunk(dc);
    }
 
    // close file streams
    fclose(stream);
    if (fileexists("tmp.txt")) fclose(backupfile);
}

// find a datachunk
datachunk *findchunk(datafile *df, const char *name) {
    datachunk *dc;
    // printf("\nSearching for datachunk %s\n",name);
    for (unsigned int i = 0; i < df->chunknumber; i++) {
        dc = df->datachunks[i];
        // printf("> Polling Datachunk #%d (%s): ", i + 1, dc->name);
        if (strncmp(dc->name,name,strlen(name)) == 0) { 
	      // printf("Success\n");
              return dc;
	}
	// printf("Failure\n");
    }

    // printf("Failed to find %s\n\n", name);
    return NULL;
}

// load a datachunk
void loadchunk(datachunk *chunk, datafile *file, FILE *stream) {
//   puts("Beginning chunk load");

    image_t *img;
    map_t *map;

    // if the chunk doesn't exist, return
    if (!chunk) return;

    // if the chunk has an error, return
    if (chunk->status == 0) return;

    // if the chunk is already loaded, our job is done, return
    if (chunk->status == 2) return;

    // keep position in memory
    unsigned long pos = checkftell(stream);

    // seek to the position specified in the datachunk
    if (fseek(stream,chunk->pos,SEEK_SET) != 0) error("Unable to seek to datachunk",true);

    // run the appropriate run function 
    switch (chunk->datatype) {
        case 48:
	    img = loadimg(stream);
	    chunk->data.img = img;
	    break;
	case 49:
	    map = loadmap(stream, file);
	    chunk->data.mp = map;
	    break;
	default:
	    error("Unable to resolve invalid datatype",false);
	    break;
    }

    //puts("Done casting");

    // set status to loaded
    chunk->status = 2;

    // seek back in the stream
    checkfseek(stream, pos, SEEK_SET);
}

// save a chunk
void savechunk(datachunk *chunk, datafile *file, FILE *stream) {
    printf("Saving chunk %s\n",chunk->name);

    image_t *img;
    map_t *map;
    unsigned long pos;

    // same song and dance as before
    if (!chunk) return;

    // if the chunk isn't loaded, return
    if (chunk->status != 2) return;

    // get current file pos
    pos = checkftell(stream);

    // set pos to chunk pos
    chunk->pos = pos;

    // save the chunk
    switch (chunk->datatype) {
        case 48:
	     img = chunk->data.img;
	     saveimg(img,stream);
	     break;
	case 49:
	     map = chunk->data.mp;
	     savemap(stream,map);
	     break;
	default:
	     error("Unable to save invalid datatype",false);
	     break;
    }
}

// unload a chunk
void unloadchunk(datachunk *chunk) {
        image_t *img;
        map_t *map;

        // same song and dance as the song and dance as before
        if (!chunk) return;

        // if the chunk isn't loaded, return
        if (chunk->status != 2) return;

        // delete THE CHUNK'S DATA
        switch (chunk->datatype) {
            case 48:
	        img = chunk->data.img;
		deleteimg(img);
		chunk->data.img = NULL;
		break;
	    case 49:
		map = chunk->data.mp;
		deletemap(map);
		chunk->data.mp = NULL;
		break;
	    default:
		error("Unable to unload invalid datatype",false);
		break;
        }

	chunk->status = 1;
}

// load chunks by name
datachunk *loadnamedchunk(datafile *file, const char *name, FILE *stream) {
  datachunk *dc = findchunk(file, name);
  loadchunk(dc, file, stream);
  return dc;
}

void savenamedchunk(datafile *file, const char *name, FILE *stream) {
  datachunk *dc = findchunk(file, name);
  savechunk(dc, file, stream);
}

void unloadnamedchunk(datafile *file, const char *name) {
  datachunk *dc = findchunk(file, name);
  unloadchunk(dc);
}
