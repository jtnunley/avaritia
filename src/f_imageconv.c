// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see f_imageconv.h for more information

#include "f_imageconv.h"
#include "i_err.h"
#include "i_file.h"
#include "i_types.h"
#include "i_salloc.h"
#include "i_string.h"

#include <setjmp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// libjpeg and libpng
#include <png.h>
#include <jpeglib.h>
#include <jerror.h>

// magick libs
// #include <wand/MagickWand.h>

#define IMAGE_LOAD_ERROR(msg) { img->err = true; \
                         img->name = strdup(msg); \
                         return img; }

// get img from map
image_t *fromcolormap(image_t *img, byte *colormp, int width, int height, byte coefficient) {
  int x, y, index;
  
  // setup image struct
  img->width = width;
  img->height = height;
  //  FILE *fp;

  // get all colors
  // NOTE: not that memory friendly, optimize later?
  color_t *clrlist = checkalloc(sizeof(color_t) * 20);
  byte clrcount = 0;
  for (y = 0; y < height; y++) {
    for (x = 0; x < width; x++) {
      index = ((y * width) + x) * coefficient;
      // push color value and check
      color_t clr = {colormp[index],colormp[index + 1],colormp[index + 2]};
//      printf("Read color {%d, %d, %d}\n",clr.red,clr.green,clr.blue);
      bool foundmatch = true;
      for (byte i = 0; i < clrcount; i++) {
        color_t existing = clrlist[i];
//	printf("Comparing with {%d, %d, %d}... ",existing.red,existing.green,existing.blue);
	if (clr.red == existing.red && clr.green == existing.green && clr.blue == existing.blue) {
          foundmatch = false;
//	  printf("Positive\n");
          break;
 	}
//	printf("Negative\n");
      }

      if (foundmatch) {
//	puts("Adding color to color list");
        clrlist[clrcount].red = clr.red;
	clrlist[clrcount].green = clr.green;
	clrlist[clrcount].blue = clr.blue;
        clrcount++;
        if (clrcount > 16) IMAGE_LOAD_ERROR("Image contains too many individual colors");
      }
    }
  }

  // set image pallete data
  img->palletelen = clrcount;
  //printf("# of colors is %d\n",clrcount);
  img->palletedata = checkalloc(sizeof(color_t) * clrcount);
  for (byte i = 0; i < clrcount; i++) {
    img->palletedata[i] = clrlist[i]; 
  }
  free(clrlist);

  // convert color data from image into LIT format
  img->pixeldata = checkalloc(sizeof(byte) * img->width * img->height);
  for (y = 0; y < height; y++) {
    for (x = 0; x < width; x++) {
      index = ((y * width) + x) * coefficient;
      color_t clr = {colormp[index],colormp[index + 1],colormp[index + 2]};
      byte colorID = 200;
      for (byte i = 0; i < img->palletelen; i++) {
        color_t existing = img->palletedata[i];
	//printf("Comapring {%d, %d, %d} and {%d, %d, %d}\n",clr.red,clr.green,clr.blue,existing.red,existing.green,existing.blue);
        if (clr.red == existing.red && clr.green == existing.green && clr.blue == existing.blue) { 
	  colorID = i + 1;
	  //printf("Assigning id %d\n",i);
	  break;
	}
      }
      if (colorID == 200) IMAGE_LOAD_ERROR("Internal process error");

      img->pixeldata[(y * width) + x] = colorID;
    }
  }
  free(colormp);

  // return the image
  return img;
}

// some information needed for jpeg error handling
struct litjpg_err_mgr {
  struct jpeg_error_mgr pub;
  jmp_buf setjmp_buffer;
};
typedef struct litjpg_err_mgr * litjpg_err;

void litjpg_err_exit(j_common_ptr cinfo) {
  litjpg_err err = (litjpg_err)(cinfo->err);
  (*cinfo->err->output_message)(cinfo);
  longjmp(err->setjmp_buffer,1);
}

// loadjpg function derived from https://github.com/LuaDist/libjpeg/blob/master/example.c
image_t *loadjpg(const char *filename) {
  // variables needed for loading process
  image_t *img = checkalloc(sizeof(image_t));
  struct jpeg_decompress_struct cinfo;

  FILE *fp;
  // JSAMPARRAY buffer;
  // int row_stride;
  struct litjpg_err_mgr jerr;
  byte coefficient = 3;

  fp = fopen(filename,"rb");
  if (!fp) IMAGE_LOAD_ERROR("Unable to open file stream"); 

  // setup err hander
  cinfo.err = jpeg_std_error(&jerr.pub);
  jerr.pub.error_exit = litjpg_err_exit;

  // establish jump
  if (setjmp(jerr.setjmp_buffer)) {
    jpeg_destroy_decompress(&cinfo);
    IMAGE_LOAD_ERROR("Internal JPEG error");
  }

  // create JPEG decompressor
  jpeg_create_decompress(&cinfo);

  // specify file source
  jpeg_stdio_src(&cinfo,fp);

  // read file parameters
  jpeg_read_header(&cinfo,true);

  // start decompression
  jpeg_start_decompress(&cinfo);

  // setup read
  coefficient = cinfo.num_components;
  int width = cinfo.output_width;
  int height = cinfo.output_height;

  // read scanlines
  byte *data = checkalloc(sizeof(byte) * width * height * coefficient);
  byte *rowptr[1];
  while (cinfo.output_scanline < cinfo.output_height) {
    rowptr[0] = data + (coefficient * cinfo.output_width * cinfo.output_scanline);
    jpeg_read_scanlines(&cinfo,rowptr,1);
  }

  jpeg_finish_decompress(&cinfo);
  fclose(fp);

  // get image from color map
  return fromcolormap(img,data,width,height,coefficient);
}

void doNothing(int i) {
  if (false == true) {
    fprintf(stderr,"Fatal logic error: %i\n",i);
  }
}

// loadpng function derived from http://www.libpng.org/pub/png/book/chapter13.html
image_t *loadpng(const char *filename) {
  // variables needed for loading process
  int x, y;
  double gamma;

  unsigned int width, height;
  int color_type;
  int bit_depth;

  png_structp png_ptr;
  png_infop info_ptr;
  int number_of_passes;
  png_bytep * row_pointers;
  FILE *fp;

  image_t *img = checkalloc(sizeof(image_t));

  fp = fopen(filename,"rb");

  // init ptr
  png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
  if (!png_ptr)
    IMAGE_LOAD_ERROR("Unable to create a png reading structure");   

  info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr) {
    png_destroy_read_struct(&png_ptr, NULL, NULL);
    fclose(fp);
    IMAGE_LOAD_ERROR("Unable to create information structure");
  }

  if (setjmp(png_jmpbuf(png_ptr))) {
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
    fclose(fp);
    IMAGE_LOAD_ERROR("Jump error triggered");
  }

  png_init_io(png_ptr, fp);
  png_set_sig_bytes(png_ptr, 0);  

  png_read_info(png_ptr, info_ptr);  // read information up to image data

  png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type,
      NULL, NULL, NULL);

  // check width and height to ensure it's within appropriate limits
  if (width > 200 || height > 200)
    IMAGE_LOAD_ERROR("Width and height of image exceeds limits");

  number_of_passes = png_set_interlace_handling(png_ptr);
  png_read_update_info(png_ptr, info_ptr);
  doNothing(number_of_passes);

  // read file
  if (png_get_gAMA(png_ptr, info_ptr, &gamma))
        png_set_gamma(png_ptr, 1.0, gamma);

  row_pointers = (png_bytep*) checkalloc(sizeof(png_bytep) * height);
  for (y=0; y<height; y++)
    row_pointers[y] = (png_byte*) checkalloc(png_get_rowbytes(png_ptr,info_ptr));

  png_read_image(png_ptr, row_pointers);

  fclose(fp);

  // setup image struct
  img->width = width;
  img->height = height;

  // coefficient
  byte coefficient = (png_get_color_type(png_ptr, info_ptr) == PNG_COLOR_TYPE_RGB ? 3 : 4);

  // get all colors
  // NOTE: not that memory friendly, optimize later?
  byte *clrlist = checkalloc(sizeof(byte) * width * height * coefficient);
  int index;
  for (y = 0; y < height; y++) {
    png_byte* row = row_pointers[y];
    for (x = 0; x < width; x++) {
      png_byte* ptr = &(row[x*coefficient]);
      //printf("Pixel at position [ %d - %d ] has RGBA values: %d - %d - %d - %d\n", x, y, ptr[0], ptr[1], ptr[2], ptr[3]);

      // push color value and check
      index = ((y * width) + x) * 3;
      clrlist[index] = ptr[0];
      clrlist[index+1] = ptr[1];
      clrlist[index+2] = ptr[2];
    }
  }

  // convert from the color map
  return fromcolormap(img,clrlist,width,height,3);
}


image_t *loadimgexternal(const char *filename) {
  image_t *img = checkalloc(sizeof(image_t));
  img->err = false;
  FILE *fp;
  // determine if the file exists
  // printf("File exists: %s\n",(fileexists(filename) ? "true" : "false"));
  if (!(fileexists(filename))) IMAGE_LOAD_ERROR("File does not exist");

  // read the magic bytes to determine if this is a jpg file or a png file

  // .jpg:  FF D8 FF
  // .png:  89 50 4E 47 0D 0A 1A 0A
  
  // read bytes
  fp = fopen(filename,"rb");
  if (!fp) IMAGE_LOAD_ERROR("Unable to create file stream"); 
  const byte max_buffer_size = 8;
  char buffer[max_buffer_size];
  if (fread(buffer,sizeof(char),8,fp) != 8) IMAGE_LOAD_ERROR("File cannot be read from");
  fclose(fp);

  // compare bytes
  if (strncmp(buffer,"\xFF\xD8\xFF",3) == 0) {
    free(img);
    return loadjpg(filename);
  }
  else if (strncmp(buffer,"\x89\x50\x4E\x47\x0D\x0A\x1A\x0A",8) == 0) {
    free(img);
    return loadpng(filename);
  }

  // throw a formatting error
  IMAGE_LOAD_ERROR("File is of an unknown/unsupported type (only png and jpg are supported)");
}
