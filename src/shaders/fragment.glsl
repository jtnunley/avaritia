#version 330 core

// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

out vec3 fragmentColor;

in vec3 vertexColor;
in vec2 texCoord;

uniform sampler2D theTexture;

void main() {
  fragmentColor = texture(theTexture, texCoord).rgb;
  // fragmentColor = vertexColor;
}
