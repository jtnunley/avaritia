#version 330 core
#extension GL_ARB_explicit_uniform_location : enable

// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

layout (location = 0) in vec3 vertexPos;
layout (location = 1) in vec3 fColor;
layout (location = 2) in vec2 aTexCoord;
layout (location = 3) uniform int wwidth;
layout (location = 4) uniform int wheight;
layout (location = 5) uniform int origwidth;
layout (location = 6) uniform int origheight;
layout (location = 7) uniform int globsperscreenW;
layout (location = 8) uniform int globsperscreenH;

out vec3 vertexColor;
out vec2 texCoord;

void main() {
  gl_Position.x = vertexPos.x;
  gl_Position.y = 0.0f - vertexPos.y;
  gl_Position.z = 0.0;
  gl_Position.w = 1.0;

  // float x = vertexPos.x + 0.5;
  vertexColor = fColor;
  texCoord = aTexCoord;
}



