// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see e_entity.h for more information

#include "e_entity.h"
#include "i_byte.h"
#include "i_err.h"
#include "i_file.h"
#include "i_random.h"
#include "i_salloc.h"
#include "i_string.h"
#include "i_types.h"

#include <string.h>

#define ENT_LOAD_ERROR(msg) { ent->err = true; \
	                      checkfree(ent->name); \
	                      ent->name = strdup(msg); \
	                      return ent; }

#define MAX_BUFFER_SIZE 256

entity *loadent(FILE *stream, datafile *df, unsigned short x, unsigned short y) {
  // ensure stream is valid
  if (!stream) error("Attempted to load entity from null stream", true);

  // initialize entity
  entity *ent = checkalloc(sizeof(entity));
  ent->err = false;

  // read magic bytes
  char buffer[MAX_BUFFER_SIZE];
  checkfgets(buffer, MAX_BUFFER_SIZE, stream);
  if (strncmp(buffer, "LIT51", 5) != 0) ENT_LOAD_ERROR("File is not a valid entity");

  // read id
  checkfgets(buffer, MAX_BUFFER_SIZE, stream);
  if (strlen(buffer) < 5) ENT_LOAD_ERROR("Entity does not have a valid id");
  ent->name = strdup(buffer);

  // read version
  byte version;
  checkfread(&version, 1, 1, stream);

  // read health, combat and walking properties bytes
  byte bytebuffer[4];
  checkfread(bytebuffer, 1, 3, stream);
  ent->health = bytebuffer[0];
  ent->walkingproperties = bytebuffer[1];
  ent->combatproperties = bytebuffer[2];

  // read to newline
  checkfgets(buffer, 5, stream);

  // read width and height of hitbox
  unsigned short shortbuffer[3];
  checkfread(shortbuffer, sizeof(unsigned short), 2, stream);
  ent->width = shortbuffer[0];
  ent->height = shortbuffer[1];

  checkfgets(buffer, 5, stream);

  // read walking properties
  if (evalByteAt(ent->walkingproperties, 3)) {
    // read distance to follow player at
    checkfread(&(ent->walkingdata.followdistance), 1, 1, stream);
  }
  if (evalByteAt(ent->walkingproperties, 5)) {
    // read radius to wander within
    checkfread(&(ent->walkingdata.walkradius), 1, 1, stream);
  }
  if (evalByteAt(ent->walkingproperties, 6)) {
    // read length of path
    checkfread(&(ent->walkingdata.pathdata.pathlen), sizeof(unsigned short), 1, stream);

    // read path
    byte dirdata;
    direction dir;
    for (unsigned short i = 0; i < ent->walkingdata.pathdata.pathlen; i++) {
      checkfread(&(dirdata), 1, 1, stream);

      // evaluate data
      switch (dirdata) {
        case 0: dir = left; break;
        case 1: dir = right; break;
	case 2: dir = up; break;
	case 3: dir = down; break;
	default: ENT_LOAD_ERROR("Unable to interpret entity path");
      }

      ent->walkingdata.pathdata.pathlen = dir;
    }
  }

  // read walking timer (even on idle ents)
  checkfread(&(ent->walktimer), sizeof(unsigned short), 1, stream);
  checkfgets(buffer, 5, stream);

  // TODO: dialogue, weapons, stats, faction, etc
  
  ent->x = x;
  ent->y = y;

  // read dependency count
  checkfread(&(ent->depCount), 1, 1, stream);
  if (ent->depCount < 2) ENT_LOAD_ERROR("Entity must have at least two dependencies");

  // read dependencies
  for (byte i = 0; i < ent->depCount; i++) {
    checkfgets(buffer, MAX_BUFFER_SIZE, stream);
    ent->dependencies[i] = strdup(buffer);
  }

  // deps 0 and 1 are walking/standing animations
  datachunk *dc = loadnamedchunk(df, ent->dependencies[0], stream);
  if (!dc) ENT_LOAD_ERROR("Walking animation not found");
  if (dc->datatype != 50) ENT_LOAD_ERROR("Walking animation not a valid animation");

  ent->walking = dc->data.anim;

  dc = loadnamedchunk(df, ent->dependencies[1], stream);
  if (!dc) ENT_LOAD_ERROR("Standing animation not found");
  if (dc->datatype != 50) ENT_LOAD_ERROR("Standing animation not a valid animation");
 
  ent->standing = dc->data.anim;

  // return the entity
  return ent;
}

void saveent(FILE *stream, entity *ent) {
  // ensure stream is valid
  if (!stream) error("Attempted to save entity to null stream", true);

  // write magic bytes + id
  fprintf(stream, "LIT51\n%s\n", ent->name);

  // write version
  byte version = CURRENT_ENT_VERSION;
  checkfwrite(&version, 1, 1, stream);
  fputs("\n", stream);

  // write health, walking, and combat bytes
  byte bytesToWrite[5];
  bytesToWrite[0] = ent->health;
  bytesToWrite[1] = ent->walkingproperties;
  bytesToWrite[2] = ent->combatproperties;
  checkfwrite(bytesToWrite, 1, 5, stream);
  fputs("\n", stream);

  // write width and height of hitbox
  checkfwrite(&(ent->width), sizeof(unsigned short), 1, stream);
  checkfwrite(&(ent->height), sizeof(unsigned short), 1, stream);
  fputs("\n", stream);

  // write walking data
  if (evalByteAt(ent->walkingproperties, 3)) {
    checkfwrite(&(ent->walkingdata.followdistance), 1, 1, stream);
  }
  if (evalByteAt(ent->walkingproperties, 5)) {
    checkfwrite(&(ent->walkingdata.walkradius), 1, 1, stream);
  }
  if (evalByteAt(ent->walkingproperties, 6)) {
    checkfwrite(&(ent->walkingdata.pathdata.pathlen), sizeof(unsigned short), 1, stream);

    // write directions
    direction dir;
    byte dirdata;
    for (unsigned short i = 0; i < ent->walkingdata.pathdata.pathlen; i++) {
      dir = ent->walkingdata.pathdata.path[i];
      dirdata = (byte) dir;
      checkfwrite(&(dirdata), 1, 1, stream);
    } 
  }

  // write walking timer
  checkfwrite(&(ent->walktimer), sizeof(unsigned short), 1, stream);
  fputs("\n", stream);

  // write dependency account, then deps
  checkfwrite(&(ent->depCount), 1, 1, stream);
  for (byte i = 0; i < ent->depCount; i++) {
    fprintf(stream, "%s\n", ent->dependencies[i]);
  }

  // TODO: other things
}

// delete entity from memory
void deletent(entity *ent) {
  if (ent) {
    checkfree(ent->name);
    deleteanim(ent->walking);
    deleteanim(ent->standing);
    checkfree(ent->walkingdata.pathdata.path);

    if (ent->dependencies) {
      for (byte i = 0; i < ent->depCount; i++) checkfree(ent->dependencies[i]);
      checkfree(ent->dependencies);
    }

    checkfree(ent);
  }
}

// deal damage to an entity
bool damageent(entity *ent, byte damage) {
  // temporarily cast health into a signed short to deal w/ negatives
  short enthealth = ent->health;

  // note: conbatproperties at 1 is whether the entity is dead, 0 is if they have been hit by the player
  if (damage == 0) return evalByteAt(ent->combatproperties, 1);
  if (evalByteAt(ent->combatproperties, 1)) return true;

  enthealth -= damage;

  if (enthealth <= 0) {
    ent->health = 0;
    ent->combatproperties = setByteVal(ent->combatproperties, 1, true);
  } else ent->health = enthealth;

  return evalByteAt(ent->combatproperties, 1);
}

// run a tick on an entity
void runtick(entity *ent) {
  // ensure the entity is not null
  if (!ent) return;

  // TODO: other things, probably

  // check to see if its time to move
  time_t currenttime = time(NULL);
  if (currenttime - ent->startingtime >= ent->walktimer) {
    ent->startingtime = currenttime;

    // determine which way to go
    if (evalByteAt(ent->walkingproperties, 0)) {
      // nothing happens
    } else if (evalByteAt(ent->walkingproperties, 1)) {
      // change direction to look at player
      // TODO: render that
    } else if (evalByteAt(ent->walkingproperties, 2)) {
      // wander randomly
            
    }
  }
}
