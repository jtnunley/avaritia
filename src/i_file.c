// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see i_file.h for more information

#include "c_unistd.h"
#include "i_err.h"
#include "i_file.h"
#include "i_types.h"
#include "i_salloc.h"
#include "i_string.h"

#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>

// read bytes from file
short readbytes(byte *buffer, FILE *stream, short count) {
  // turns out it was easier than I thought
  return fread(buffer, sizeof(byte), count, stream);
}

// read string from file
void readstring(char *buffer, FILE *stream, short *length) {
  (*length) = fscanf(stream,"%[^\n]",buffer); 

  // cut off the newline at the end bc it sucks
  buffer[(*length) - 1] = '\0';
}

// write bytes to file
void writebytes(byte *buffer, FILE *stream, short count) {
  if (fwrite(buffer,sizeof(byte),count,stream) != count) error("Unable to write bytes to file",true);
  buffer[count] = '\0';
}

// write string to file
void writestring(char *buffer, FILE *stream) {
  char current;
  short index = 0;
  byte *b = malloc(sizeof(byte) * 5);
  do {
    current = buffer[index++];
    b[0] = current;
    writebytes(b,stream,1);
  } while (current != '\n' && current != 0);
  free(b);
}

void checkfwrite(const void *buffer, size_t size, size_t len, FILE *stream) {
  if (fwrite(buffer,size,len,stream) != len) error("Unable to fwrite to file",true);
}
void checkfread(void *buffer, size_t size, size_t len, FILE *stream) {
  // printf("File position: %ld, reading %ld bytes from a file of size %ld\n", checkftell(stream), size * len, getfilesize(stream));
  if (fread(buffer,size,len,stream) != len) error("Unable to read from file",true);
}

void checkremove(const char *filename) {
  if (remove(filename) != 0) error("Unable to remove file",true);
}

// check if file exists
bool fileexists(const char *filename) {
  if (access(filename,F_OK) != -1) return true;
  else return false;
}

// safe fgets
void checkfgets(char *buffer, short maxbytes, FILE *stream) {
  if (fgets(buffer,maxbytes,stream) == NULL) {
    error("Unable to read line from file",true);
  }
  removenewline(buffer);
}

// checked fseek
void checkfseek(FILE *stream, long offset, int origin) {
  if (fseek(stream, offset, origin) != 0) error("Unable to change position of cursor in file", true);
}

// get the size of a file 
long getfilesize(FILE *stream) {
  long previouspos = ftell(stream);
  checkfseek(stream,0,SEEK_END);

  long pos = ftell(stream);
  if (pos == -1L) error("Unable to get position of cursor in file",true);

  checkfseek(stream,previouspos,SEEK_SET);
  return pos;
}

// are we at end of file?
bool atendoffile(FILE *stream) {
  return (feof(stream) != 0);  
}

// read the entire file into a string
char *readentirefile(const char *filename) {
  // firstly, check to see if the file exists
  if (!(fileexists(filename))) {
    // create the error message
    const char *format = "File %s does not exist";
    byte errMsgLen = strlen(format) - 1 + strlen(filename);
    char *errMsg = checkalloc(sizeof(char) * errMsgLen);
    sprintf(errMsg,format,filename);

    error(errMsg,false);
  }

  // open stream
  FILE *stream = fopen(filename,"rb");

  // get file length
  long length = getfilesize(stream);

  // allocate buffer and fread in file
  char *str = checkalloc(sizeof(char) * (length + 1));
  if (fread(str,sizeof(char),length,stream) != length) error("Unable to read from file",true);

 // printf("Read file %s: \n%s\nEnd file %s\n",filename,str,filename);
 
  // add null terminator
  str[length] = '\0';

  fclose(stream);

  // return the string
  return str;
}

// copy a file from one location to another
void copyfile(const char *filename1, const char *filename2) {
  FILE *stream1 = fopen(filename1,"r");
  if (!stream1) {
    error("Unable to open file for copying",true);
  }
  FILE *stream2 = fopen(filename2,"w");
  if (!stream2) {
    fclose(stream1);
    error("Unable to open file for copying",true);
  }

  // copy over chars
  char ch;
  byte bytes;
  while (!feof(stream1)) {
    bytes = fread(&ch,1,1,stream1);
    if (bytes) checkfwrite(&ch,1,bytes,stream2);
  }

  // close files
  fclose(stream1);
  fclose(stream2);
}

// get stream position
unsigned long checkftell(FILE *stream) {
  long pos = ftell(stream);
  if (pos < 0) error("Unable to get position of cursor in file",true);
  return pos;
}
