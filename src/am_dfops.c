// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see am_dfops.h for more information

#include "am_dfops.h"
#include "am_interactive.h"
#include "f_datafile.h"
#include "f_image.h"
#include "i_byte.h"
#include "i_err.h"
#include "i_file.h"
#include "i_salloc.h"
#include "i_string.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// basic mini-func
#define MIN(a,b) (a < b ? a : b)
#define MAX(a,b) (a < b ? b : a)
#define MAX_BUFFER_SIZE 256

// TODO: find a more sophisticated way to find the tmp dir

#define SUMMARIZE_DATACHUNK(dc) printf("-> %s ",dc->name); \
      printf("(LIT%d)", (int)(dc->datatype)); \
      if (dc->status == 2) printf(" [LOADED]"); \
      puts("");

// summarize datafile
void summarizedatafile(datafile *df) {
  printf("%s (LIT01):\n",df->name);
  if (df->chunknumber > 0) {
    for (unsigned short i = 0; i < MIN(df->chunknumber,7); i++) { // datachunkslen or 10
      SUMMARIZE_DATACHUNK(df->datachunks[i]); 
    }
    if (df->chunknumber > 10) {
      printf("-> (%d more...)\n",df->chunknumber - 10);
    }
    if (df->chunknumber > 8) {
      unsigned short length = MIN(3,df->chunknumber - 7); 
      for (byte i = 0; i < length ; i++) {
        SUMMARIZE_DATACHUNK(df->datachunks[df->chunknumber + i - length]);
      }
    } 
  } else {
    printf("-> (nothing...)\n");
  }
}

// create an image
void am_createimg(datafile *df) {
  byte width, height, palleteLen;
  char buffer[MAX_BUFFER_SIZE];
  char *name;

  // get name of image
  while (true) {
    printf("Enter name of image: ");
    checkfgets(buffer,MAX_BUFFER_SIZE,stdin);
    
    if (strlen(buffer) < 5) printf("Name must be at least five characters in length: ");
    else {
      name = checkalloc(sizeof(char) * (strlen(buffer) + 1));
      strcpy(name,buffer);
      //printf("Name is %s\n",name);
      break;
    }
  }

  

  // get image width and height
  #define GET_IMAGE_DIM(title,var) printf("Input the %s of the image: ",title); \
                               while (true) { \
                                 checkfgets(buffer,MAX_BUFFER_SIZE,stdin); \
                                 if (!(isstrnumber(buffer))) { \
                                   printf("Please input a number: "); \
			           continue; \
			         } \
                                 var = (byte)(atoi(buffer)); \
                                 if (var <= 0) { \
			            printf("The %s must be greater than zero: ",title); \
			            continue; \
			         } \
				 else break; \
                               }

  GET_IMAGE_DIM("width",width);
  GET_IMAGE_DIM("height",height);
  GET_IMAGE_DIM("palette length",palleteLen);

  // create the datachunk
  datachunk *datach = checkalloc(sizeof(datachunk));

  // making sure datach->name is set right 
  datach->name = strdup(name); 
  if (strncmp(datach->name,name,strlen(name)) != 0) error("strdup failed to duplicate string", true);
  //printf("datach->name is %s\n",datach->name);
  
  datach->pos = df->currentpos;
  datach->status = 2;
  datach->dependencynumber = 0;
  datach->datatype = 48;
  //printf("datach->name is %s\n",datach->name);


  // create the image
  image_t *img = checkalloc(sizeof(image_t));
  img->width = width;
  img->height = height;
  //puts("Done w/ w/h init");
  img->name = strdup(name); 
  //puts("Done with initialization of width/height/name");
  img->palletelen = palleteLen;
  img->palletedata = checkalloc(sizeof(color_t) * palleteLen);
  for (byte i = 0; i < img->palletelen; i++) {
    color_t clr = {0,0,0};
    img->palletedata[i] = clr;
  }
  //puts("Done with pallete init");
  img->pixeldata = checkalloc(sizeof(byte) * width * height);
  //puts("Beginning fill of pixeldata");
  for (short i = 0; i < width * height; i++) {
    img->pixeldata[i] = 1;
  }

  img->version = CURRENT_IMAGE_VERSION;
  // printf("datach->name is %s\n",datach->name);

  // we can write the image to and from a temporary file to determine the length
  FILE *tempfile = fopen("tmp.txt","wb");
  saveimg(img,tempfile);
  df->currentpos += getfilesize(tempfile);
  fclose(tempfile);
  remove("tmp.txt");

  //puts(datach->name);
  //puts(datach->id);

  // set image data
  //printf("datach->name is %s\n",datach->name);
  datach->data.img = img;

  // add the datachunk to the datafile
  df->datachunks[df->chunknumber] = datach;
  df->chunknumber += 1;

  //printf("After initialization, name is %s, datach->name is %s, and img->name is %s\n",name,datach->name,img->name);

  // run modification subroutine
  // printf("datach->name is %s\n",datach->name);
  am_modifyimg(df,name);
  free(name);
}

// modify an existing image chunk
void am_modifyimg(datafile *df, char *name) {
  FILE *stream = fopen(df->filename,"rb");

  datachunk *dc = loadnamedchunk(df,name,stream);
  if (!dc) {
    error("loaddatachunk() returned 0, something is wrong",false);
  }
  image_t *img = dc->data.img;
  if (!img) error("datachunk did not contain image",false);

  if (stream) fclose(stream);

  char buffer[MAX_BUFFER_SIZE];
  bool continueIMGLoop = true;

  char options[] = {'a','b','c','d','e','f','q'};
  char *descriptions[] = {"Change image name",
	                  "Add new color",
			  "Remove color",
			  "Change color",
			  "Set color to alpha",
			  "Modify image data",
			  "Return to AssetManager"};
  byte optNum = 7;
  while (continueIMGLoop) {
beginningofIMGLoop: // label because the loop sometimes doesn't work
    puts("");
    // summarize image
    printf("%s [%d, %d] (LIT48): \n",img->name,img->width,img->height);
    for (byte i = 0; i < img->palletelen; i++) {
      color_t clr = img->palletedata[i];
      if (IS_COLOR_ALPHA(clr))
        printf("-> Color #%d [alpha]\n",i+1);
      else
        printf("-> Color #%d [%d, %d, %d]\n",i+1,clr.red,clr.green,clr.blue);
    }
    puts("");

    // get user input
    char answer = createmenu(options,descriptions,optNum);
    byte r,g,b;
    byte index,currentIndex = 0;
    color_t *newpallete;
    switch(answer) {
      case 'a':
	printf("Input image name: ");
	while (true) {
          checkfgets(buffer,MAX_BUFFER_SIZE,stdin);
	
	  if (strlen(buffer) < 5) printf("Image name must be five characters in length or more: ");
	  else {
            //free(img->name);
	    img->name = checkalloc(sizeof(char) + (strlen(buffer) + 1));
	    strcpy(img->name,buffer);
	    //free(dc->name);
	    dc->name = checkalloc(sizeof(char) + (strlen(buffer) + 1));
	    strcpy(dc->name,buffer);
	    break;
	  } 
	}
	break;
      case 'b':
        readcolor(&r,&g,&b); // read a color	
	
	// create a new pallete data
	newpallete = checkalloc(sizeof(color_t) * (img->palletelen + 1));
	for (byte i = 0; i < img->palletelen; i++) {
          newpallete[i] = img->palletedata[i];
	}
	newpallete[img->palletelen].red = r;
	newpallete[img->palletelen].green = g;
	newpallete[img->palletelen].blue = b;

	// replace old pallete data
        free(img->palletedata);
        img->palletedata = newpallete;
	img->palletelen += 1;

	puts("");
	goto beginningofIMGLoop;
	break;
      case 'c':
	// get index to remove
	index = getnumber("Input index of color to remove: ");
	index--; // indexing starts at 0

	newpallete = checkalloc(sizeof(color_t) * (img->palletelen - 1));
	currentIndex = 0;
	for (byte i = 0; i < img->palletelen; i++) {
          if (i != index) newpallete[currentIndex] = img->palletedata[i];
	  currentIndex++;
	}
	free(img->palletedata);
	img->palletedata = newpallete;
	img->palletelen -= 1;

	puts("");
        goto beginningofIMGLoop;
        break;
      case 'd':
	// get index to change	
	index = getnumber("Input index of color to change: ");
	readcolor(&r,&g,&b);

	img->palletedata[index-1].red = r;
	img->palletedata[index-1].green = g;
	img->palletedata[index-1].blue = b;

	puts("");
        goto beginningofIMGLoop;
	break;
      case 'e':
	printf("Input index of color to set to alpha: ");
        index = getnumber("Input index of color to set to alpha: ");
	
	img->palletedata[index-1].red = C_ALPHA_RED;
	img->palletedata[index-1].green = C_ALPHA_GREEN;
	img->palletedata[index-1].blue = C_ALPHA_BLUE;
	goto beginningofIMGLoop;
	break;
      case 'f':
	am_imagedata(img);
	goto beginningofIMGLoop;
	break;
      case 'q':
	continueIMGLoop = false;
	break;
    }
  }
}

#define WH_TO_INDEX(x,y,width) (y*width) + x

// display image data
void am_displayimagedata(image_t *img) {
  puts("");
  for (byte i = 0; i < img->height; i++) {
    for (byte j = 0; j < img->width; j++) {
      printf("%d",img->pixeldata[WH_TO_INDEX(j,i,img->width)]);     
    }
    puts("");
  }
  puts("");
}

// modify the image data proper
void am_imagedata(image_t *img) {
  char buffer[MAX_BUFFER_SIZE];
  byte x1,x2,y1,y2,palleteCode = 0;
  char **stringparts;
  unsigned short count;
  while (true) {
    am_displayimagedata(img);

    // prompts
    puts("Type three numbers (x, y, pallete code) to change a single pixel.");
    puts("Type five numbers (x1, y1, x2, y2, pallete code) to change a rectangle.");
    puts("(Indices for x and y coordinates go from [0, 0] (top-left corner) to [width-1,height-1] (bottom-right corner))");
    puts("Type 'q' to quit.");
    printf("> ");

    while (true) {
      checkfgets(buffer,MAX_BUFFER_SIZE,stdin);
     
      if (strlen(buffer) <= 0) {
        printf("Invalid input: ");
	continue;
      }
      
      stringparts = splitstring(buffer,' ',&count);
      
      if (count != 3 && count != 5 && (count != 1 && (stringparts[0])[0] != 'q')) {
        printf("Invalid, enter another option: ");
	continue;
      }
      
      if (count == 3 || count == 5) {
	// flag to continue loop
	bool continueflag = false;
        for (byte i = 0; i < count; i++) {
          if(!isstrnumber(stringparts[i])) {
            printf("Please enter a valid number: ");
	    continueflag = true;
	    break;
	  }

	  // test number
	  short testnum = (short)atoi(stringparts[i]);
	  if (testnum < 0) {
            printf("Please enter a number greater than or equal to zero: ");
	    continueflag = true;
	    break;
	  }
        }
	if (continueflag) continue;
      }

      if (count != 1 && ((count == 3 ? atoi(stringparts[2]) : atoi(stringparts[4])) > img->palletelen)) {
        printf("Please enter a valid index: ");
	continue;
      }

      break;
    }

    // process information
    if (count == 1) break;
    else if (count == 3) {
      x1 = atoi(stringparts[0]);
      y1 = atoi(stringparts[1]);
      palleteCode = atoi(stringparts[2]);

      img->pixeldata[WH_TO_INDEX(x1,y1,img->width)] = palleteCode;
    } else if (count == 5) {
      x1 = atoi(stringparts[0]);
      y1 = atoi(stringparts[1]);
      x2 = atoi(stringparts[2]);
      y2 = atoi(stringparts[3]);
      palleteCode = atoi(stringparts[4]);

      for (byte j = y1; j <= y2; j++)
        for (byte i = x1; i <= x2; i++)
	  img->pixeldata[WH_TO_INDEX(i,j,img->width)] = palleteCode;
    }

    free(stringparts);
    puts("");
  }

  //puts("end of am_imagedata loop");
}

// create a map
void am_createmap(datafile *df) {
  map_t *map = checkalloc(sizeof(map_t));
  map->version = CURRENT_MAP_VERSION;

  // get map name
  char buffer[MAX_BUFFER_SIZE];

  printf("Enter name of map: ");
  while (true) {
    checkfgets(buffer,MAX_BUFFER_SIZE,stdin);
    if (strlen(buffer) < 5) printf("Please enter a valid id: ");
    else break;
  } 
 
  // copy to mp->name
  map->name = strdup(buffer);

  // get width and height
  printf("Enter width of map: ");
  map->width = getnumber("Enter width of map: ");
  printf("Enter height of map: ");
  map->height = getnumber("Enter height of map: ");

  map->tiles = checkalloc(sizeof(tiletype) * 3);

  // get the tile colors
  color_t color;
  byte r,g,b;

  for (byte i = 0; i < 3; i++) {
    map->tiles[i].properties = 0;
  }

  printf("Input floor tile color: ");  
  readcolor(&r, &g, &b);
  color.red = r; color.green = g; color.blue = b;
  SET_COLOR(map->tiles[0].color, color);
  map->tiles[0].properties = setByteVal(map->tiles[0].properties, 1, true); // set as floor tile

  printf("Input wall tile color: ");
  readcolor(&r, &g, &b);
  color.red = r; color.green = g; color.blue = b;
  SET_COLOR(map->tiles[1].color, color);
  map->tiles[1].properties = setByteVal(map->tiles[1].properties, 2, true); // set as wall tile

  printf("Input OOB tile color: ");
  readcolor(&r, &g, &b);
  color.red = r; color.green = g; color.blue = b;
  SET_COLOR(map->tiles[2].color, color);
  map->tiles[2].properties = setByteVal(map->tiles[2].properties, 3, true); // set as OOB tile

  map->tilenumber = 3;

  // define the internal image 
  image_t *img;
  datachunk *imgdc = NULL;
  bool createdImage = false;
  printf("Input the name of the internal image: ");
  while (true) {
    checkfgets(buffer,MAX_BUFFER_SIZE,stdin);
    if (strlen(buffer) < 5) printf("Please input a valid id: ");
    else break;
  }

  if (fileexists(df->filename)) {
      FILE *imgLoadingStream = fopen(df->filename, "rb");
      imgdc = loadnamedchunk(df, buffer, imgLoadingStream);
      fclose(imgLoadingStream);
  }

  if (!imgdc) {
    printf("Note: %s not found, creating new image...\n", buffer);
    createdImage = true;
    img = checkalloc(sizeof(image_t));
    img->name = strdup(buffer);

    // set fields for the image
    img->width = map->width;
    img->height = map->height;
    img->palletelen = 3;
    img->palletedata = checkalloc(sizeof(color_t) * img->palletelen);
    img->version = CURRENT_IMAGE_VERSION;

    // set palletedat
    SET_COLOR(img->palletedata[0], map->tiles[0].color);
    SET_COLOR(img->palletedata[1], map->tiles[1].color);
    SET_COLOR(img->palletedata[2], map->tiles[2].color);

    // set pixeldata
    img->pixeldata = checkalloc(sizeof(byte) * img->width * img->height);
    for (unsigned short i = 0; i < img->width * img->height; i++) {
      img->pixeldata[i] = 1;
    }
  } else {
    if (!(imgdc->data.img)) error("Image chunk does not have a valid image", false);
    img = imgdc->data.img;
  }

  map->pixeldata = img->pixeldata;

  // create datachunks
  datachunk *mapdc = checkalloc(sizeof(datachunk));
  if (createdImage) imgdc = checkalloc(sizeof(datachunk));

  // get positions of saved maps
  unsigned long imglength, maplength;

  // we can write the image to and from a temporary file to determine the length
  FILE *stream = fopen("tmp.txt", "wb");
  if (!stream) error("Unable to open file for writing", true);
  if (createdImage) {
     saveimg(img, stream);
     // printf("Reading entire file: %s\n", readentirefile("tmp.txt"));
     imglength = getfilesize(stream);
     // printf("imglength is %ld\n",imglength);
  }
  fclose(stream);  
  checkremove("tmp.txt");

  // initialize image datachunk (if not initialized already)
  if (createdImage) {
    imgdc->name = strdup(img->name);
    imgdc->datatype = 48;
    imgdc->status = 2;
    imgdc->data.img = img;
    imgdc->dependencynumber = 0;
    imgdc->pos = df->currentpos;
    df->currentpos += imglength;
  }

  // initialize map datachunk
  mapdc->name = strdup(map->name);
  mapdc->datatype = 49;
  mapdc->status = 2;
  mapdc->data.mp = map;
  mapdc->dependencynumber = 1;
  mapdc->pos = df->currentpos; 

  mapdc->dependencies = checkalloc(sizeof(char *) * mapdc->dependencynumber);
  mapdc->dependencies[0] = strdup(img->name);

  map->dependencynumber = 1;
  map->dependencies = checkalloc(sizeof(char *) * mapdc->dependencynumber);
  map->dependencies[0] = strdup(img->name);

  // we can write the map to and from a temporary file to determine the length
  stream = fopen("tmp.txt","wb");
  savemap(stream, map);
  maplength = getfilesize(stream);
  fclose(stream);
  checkremove("tmp.txt");

  df->currentpos += maplength;

  // put chunks into datafile
  if (createdImage) df->datachunks[df->chunknumber] = imgdc;
  df->datachunks[df->chunknumber + 1] = mapdc;
  df->chunknumber += 2;

  //modify map
  am_modifymap(df,map->name);
}
