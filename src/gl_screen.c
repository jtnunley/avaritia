// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

#include "gl_draw.h"
#include "gl_screen.h"
#include "i_salloc.h"

#include <stdlib.h>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

void modifyglob(glbuffer *buffer, unsigned short x, unsigned short y, color_t clr) {
  changecolor(buffer->buffer, x, y, clr);
}

void clearscreen(glbuffer *buffer, color_t clr) {
  for (unsigned short i = 0; i < globsperscreenW; i++) {
    for (unsigned short j = 0; j < globsperscreenH; j++)
      modifyglob(buffer, i, j, clr);
  }
}
