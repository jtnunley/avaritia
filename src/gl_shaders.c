// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see gl_shaders.h for more information

#include "gl_shaders.h"
#include "i_err.h"
#include "i_file.h"
#include "i_salloc.h"
#include "i_string.h"

#include <stdlib.h>
#include <string.h>

// check to ensure that a shader is compiled OK
void checkshader(GLuint shader, const char *shadername) {
  // some variables that must be used for shader compilation
  GLint result = GL_FALSE;
  int infologlength;

  glGetShaderiv(shader, GL_COMPILE_STATUS, &result);
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &infologlength);

  //printf("infologlength of %s is %d\n",shadername,infologlength);

  if (infologlength > 0) {
    // get error message
    char *errMsg = checkalloc(sizeof(char) * (infologlength + 1));
    glGetShaderInfoLog(shader, infologlength, NULL, errMsg);
    
    // get real error message
    const char *vErrFormat = "%s: %s";
    unsigned short realErrLen = strlen(vErrFormat) - 3 + strlen(errMsg) + strlen(shadername);
    char *rErrMsg = checkalloc(sizeof(char) * realErrLen);
    sprintf(rErrMsg, vErrFormat, shadername, errMsg);
    error(rErrMsg, false);
  }
}

// check program linking
void checkprogram(GLuint program) {
  // some variables that must be used
  GLint result = GL_FALSE;
  int infologlength;

  glGetProgramiv(program, GL_LINK_STATUS, &result);
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &infologlength);
  if (infologlength > 0) {
    puts("> Shader error found: ");
    // get error message
    char *errMsg = checkalloc(sizeof(char) * (infologlength + 1));
    glGetProgramInfoLog(program, infologlength, NULL, errMsg);
    
    // get real error message
    const char *pErrFormat = "Shader Linker: %s";
    unsigned short realErrLen = strlen(pErrFormat) - 1 + strlen(errMsg);
    char *rErrMsg = checkalloc(sizeof(char) * realErrLen);
    sprintf(rErrMsg, pErrFormat, errMsg);
    error(rErrMsg, false);
  }
}

// create a shader
GLuint createshader(GLenum type, char *src, const char *shadername) {
  //printf("> > Compiling %s\n",shadername);

  GLuint shader = glCreateShader(type);

  GLchar *src_but_glchar_pointer = (GLchar *) src;

  // compile shader source code
  glShaderSource(shader,1,&src_but_glchar_pointer,NULL);
  glCompileShader(shader);

  //puts("> > Checking shader");

  // ensure it compiled successfully
  checkshader(shader,shadername);

  printf("> %s compiled successfully\n",shadername);

  return shader;
}

#define TEST_FOR_GL_ERR(name) if (e = glGetError()) fprintf(stderr,"GL error in %s: %d\n",name,e);

GLuint loadshaders(const char *vertexfile, const char *fragmentfile) {
  // read vertex shader code
  char *vertexshadercode = readentirefile(vertexfile);
  char *fragmentshadercode = readentirefile(fragmentfile);
  // char *geometryshadercode = readentirefile(geometryfile);

  puts("> Read shaders from file, compiling shaders...");

  // create the shaders
  GLuint vertexshader = createshader(GL_VERTEX_SHADER,vertexshadercode,"Vertex Shader");
  GLuint fragmentshader = createshader(GL_FRAGMENT_SHADER,fragmentshadercode,"Fragment Shader");
  // GLuint geometryshader = createshader(GL_GEOMETRY_SHADER,geometryshadercode,"Geometry Shader");

  puts("> Shaders compiled, linking shaders into program...");

  // link the fragment and vetex shader into a program
  GLuint program = glCreateProgram();
  glAttachShader(program,vertexshader);
  glAttachShader(program,fragmentshader);
  // glAttachShader(program,geometryshader);
  glLinkProgram(program);

  // make sure it linked correclty
  checkprogram(program);

  puts("> Program is linked, deleting excess shader memory...");

  // delete the shaders - they're already compiled in
  glDetachShader(program, vertexshader);
  glDetachShader(program, fragmentshader);
  // glDetachShader(program, geometryshader);
  glDeleteShader(vertexshader);
  glDeleteShader(fragmentshader);
  // glDeleteShader(geometryshader);

  checkfree(vertexshadercode);
  checkfree(fragmentshadercode);
  // checkfree(geometryshadercode);

  // return the linked program
  return program;
}
