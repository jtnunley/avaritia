// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// this file is the entry point of the program, should just grab args and enter main game loop

// standard libary headers
#include <stdio.h>

#include "am_main.h"

#include "gl_loop.h"
#include "i_args.h"
#include "i_types.h"

// testing, remove later
#include "i_string.h"
#include <string.h>
#include <stdlib.h>

// entry point function
int main(int argc, char **argv) {
    printf("Avaritia Alpha v0.0.5\n");
    
    argc_ = argc;
    argv_ = argv;

    bool run_gl_loop = true;

#ifdef ASSETMANAGER
    if (argcheck("--assetmanager") || argcheck("--am")) {
      beginassetmanager(); // asset manager
      run_gl_loop = false;
    }
#endif
 
    if (run_gl_loop) {
      printf("Beginning main GL loop\n");
      return beginGLLoop(); // main game
    }

    return 0;
}
