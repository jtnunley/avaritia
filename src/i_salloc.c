// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see i_salloc.h for more information

#include "i_err.h"
#include "i_salloc.h"

#include <stdlib.h>

void *checkalloc(size_t size) {
  // allocate information
  void *result = malloc(size);

  // check to ensure the result exists
  if (!result) {
    error("Unable to allocate memory on heap",true);
  }

  // return result
  return result;
}

void checkfree(void *ptr) {
  if (ptr == NULL) return;
  free(ptr);
  ptr = NULL;
}
