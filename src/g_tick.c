// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

#include "g_tick.h"
#include "gl_renderer.h"
#include "gl_screen.h"
#include "i_err.h"
#include "i_salloc.h"
#include "i_types.h"
#include "im_text.h"

#include <stdio.h>

unsigned long int gameticks = 0;

void initgame(gamestate *gt, const char *textfile) {
  inittextrender(textfile);

  // load the main datafile
  char *err = checkalloc(3);
  gt->gamefile = loaddatafile(gt->filename, err);
  if (!(gt->gamefile)) {
    error(err, false);    
  }

  printf("Initmap: %s\n", gt->gamefile->initialmap);

  if (!(gt->gamefile->initialmap)) error("Game datafile does not contain initial map", false);

  FILE *stream = fopen(gt->gamefile->filename, "rb");
  gt->currentmap = loadnamedchunk(gt->gamefile, gt->gamefile->initialmap, stream)->data.mp;
  gt->mapdata = loadnamedchunk(gt->gamefile, gt->currentmap->dependencies[0], stream)->data.img;
  fclose(stream);
}

bool gametick(gamestate *gt, glbuffer *buffer) {
  gameticks++;
  char *str = checkalloc(sizeof(char) * 40);

  color_t cwhite = {255,255,255};
  color_t cblack = {0, 0, 0};

  if (gameticks > 5) {

    // for proof of concept, just mess with the screen
    // clearscreen(buffer, cblack);
    /*unsigned short adjusted_val = (gameticks - 6) % (globsperscreenW * globsperscreenH);
    unsigned short xindex = adjusted_val % globsperscreenW;
    unsigned short yindex = adjusted_val / globsperscreenW;
     
    color_t clr = {255,0,0};
    modifyglob(buffer,xindex,yindex,clr);*/

    sprintf(str,"%ld",gameticks);

    unsigned short xOffset = ((gameticks / 10) % 30);
    unsigned short yOffset =  ((gameticks / 50) % 30);
    unsigned short offset = ((gameticks / 120) % 10);
    // rendertxt(buffer, "hello world!", 10 + xOffset, 10 + yOffset,cwhite);
    // rendertxt(buffer, "larson smells...",10+xOffset,21+yOffset,cwhite);
    // rendertxt(buffer,str,10 + xOffset,10 + yOffset,cwhite);
    //clearscreen(buffer, cwhite);
    // drawimages(buffer, gt->mapdata, 0, 0, 10, 10);
    drawmap(buffer, gt->currentmap, offset, offset);
    // drawimage(buffer, gt->mapdata, 11, 11);
    // clearscreen(buffer, cwhite);

    // for (byte i = 0; i < 10; i++) {
      // for (byte j = 0; j < 10; j++) {
        // modifyglob(buffer, i, j, cwhite);
      // } 
    // }
  } 

  return true;
}
