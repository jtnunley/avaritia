// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// essentially a continuation of am_dfops.c, but I didn't want to have too long of a file

#include "am_dfops.h"
#include "am_interactive.h"
#include "f_datafile.h"
#include "f_image.h"
#include "i_byte.h"
#include "i_err.h"
#include "i_file.h"
#include "i_salloc.h"
#include "i_string.h"
#include "i_types.h"
#include "m_map.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_BUFFER_SIZE 256

#define MAP_TILETYPE_SET(t1, t2) t1.properties = t2.properties; \
	t1.color.red = t2.color.red; \
        t1.color.green = t2.color.green; \
        t1.color.blue = t2.color.blue;

// summarize a tiletype
void summarizetiletype(tiletype tt, byte i) {
  printf("Tile #%d (%d, %d, %d) - ", i + 1, tt.color.red, tt.color.green, tt.color.blue);
      
  printf("Tile properties byte is %d - ", tt.properties);

  if (evalByteAt(tt.properties, 1)) { // floor tile
    puts("Floor Tile");
  } else if (evalByteAt(tt.properties, 2)) { // wall tile
    puts("Wall Tile");
  } else if (evalByteAt(tt.properties, 3)) { // OOB tile
    puts("OOB Tile");
  }
}

// modify a tiletype
void am_modifytiletype(map_t *map, byte index) {
  // menu for tile modification
  char tOptions[] = {'a','b','c','d','e','q'};
  char *tDescriptions[] = {"Change to floor tile",
	                  "Change to wall tile",
			  "Change to OOB tile",
			  "Change tile color",
			  "Delete tile type",
			  "Return to map modification"};
  byte tOptNum = 6;

  byte r,g,b;

  bool continueTTLoop = true;
  byte i = index;
  while (continueTTLoop) {
    summarizetiletype(map->tiles[i], index); 

    char result = createmenu(tOptions, tDescriptions, tOptNum);
    if (result == 'a') {
      if (evalByteAt(map->tiles[i].properties, 1)) {
        puts("Tile is already a floor tile");
	continue;
      }

      // set to floor tile
      map->tiles[i].properties = setByteVal(map->tiles[i].properties, 1, true);

      // set wall/oob bits to false
      map->tiles[i].properties = setByteVal(map->tiles[i].properties, 2, false);
      map->tiles[i].properties = setByteVal(map->tiles[i].properties, 3, false);
    } else if (result == 'b') {
      if (evalByteAt(map->tiles[i].properties, 2)) {
        puts("Tile is already a wall tile");
	continue;
      }

      // set to wall tile
      map->tiles[i].properties = setByteVal(map->tiles[i].properties, 2, true);

      // set floor/oob bits to false
      map->tiles[i].properties = setByteVal(map->tiles[i].properties, 1, false);
      map->tiles[i].properties = setByteVal(map->tiles[i].properties, 3, false);
    } else if (result == 'c') {
      if (evalByteAt(map->tiles[i].properties, 3)) {
        puts("Tile is already an OOB tile");
	continue;
      }

      // set to oob tile
      map->tiles[i].properties = setByteVal(map->tiles[i].properties, 3, true);

      // set wall/floor bits to false
      map->tiles[i].properties = setByteVal(map->tiles[i].properties, 1, false);
      map->tiles[i].properties = setByteVal(map->tiles[i].properties, 2, false);
    } else if (result == 'd') {
      readcolor(&r, &g, &b);

      map->tiles[i].color.red = r;
      map->tiles[i].color.green = g;
      map->tiles[i].color.blue = b;
    } else if (result == 'e') {
      // TODO: tile type deletion
    } else if (result == 'q') {
      continueTTLoop = false;
    }
  } 
}

// modify a map
void am_modifymap(datafile *df, char *name) {
  puts("");

  FILE *stream = fopen(df->filename, "rb");

  datachunk *mapdc = loadnamedchunk(df, name, stream);
  if (!mapdc) error("loaddatafile returned 0",false);

  if (mapdc->datatype != 49) {
    puts("Chunk is not a map chunk, returning...");
    return;
  }

  map_t *map = mapdc->data.mp;
  if (!map) error("Loaded a null map", false);
  if (map->err) {
    printf("Map had internal error: %s\n", map->name);
    return;
  }

  //puts("Loaded map");

  datachunk *imgdc = loadnamedchunk(df,map->dependencies[0],stream);
  if (!imgdc) error("loaddatafile returned 0",false);
  image_t *img = imgdc->data.img;

  if (stream) fclose(stream);

  // menu for map modification
  char options[] = {'a','b','c','d','e','q'};
  char *descriptions[] = {"Change map name",
	                  "Add new tile type",
			  "Remove tile type",
			  "Change tile type",
			  "Modify internal data",
			  "Return to AssetManager"};
  byte optNum = 6;

  bool continueMAPLoop = true;
  char buffer[MAX_BUFFER_SIZE];
  char choice;
  color_t scolor;
  tiletype tt;
  tiletype *newttlist;
  while (continueMAPLoop) {
    // summarize the map
    printf("%s [%d, %d] (LIT49)\n",map->name,map->width,map->height);
    
    for (byte i = 0; i < map->tilenumber; i++) {
      tt = map->tiles[i];
      summarizetiletype(tt, i);   
    } 

    // make menu
    char result = createmenu(options,descriptions,optNum);
    if (result == 'a') {
      // set map id
      printf("Input new map name: ");
      while (true) {
        checkfgets(buffer,MAX_BUFFER_SIZE,stdin);
	if (strlen(buffer) < 5) printf("Please input a valid map id: ");
	else break;
      }

      // free memory, et al
      checkfree(map->name);
      map->name = strdup(buffer);
      checkfree(mapdc->name);
      mapdc->name = strdup(buffer); 

      puts("");
    } else if (result == 'b') {
      // add a new tile
      readcolor(&(scolor.red),&(scolor.green),&(scolor.blue));
      SET_COLOR(tt.color, scolor);
      
      printf("Input F for Floor tile, W for Wall tile, or O for OOB tile: ");
      while (true) {
        checkfgets(buffer, MAX_BUFFER_SIZE, stdin);
	choice = tolower(buffer[0]);
	if (choice != 'f' && choice != 'w' && choice != 'o') printf("Please input a valid option: ");
	else break;
      }

      newttlist = checkalloc(sizeof(tiletype) * (map->tilenumber + 1));
      for (byte i = 0; i < map->tilenumber; i++) {
        MAP_TILETYPE_SET(newttlist[i], map->tiles[i]);
      }
      
      tt.properties = 0;
      if (choice == 'f') tt.properties = setByteVal(tt.properties, 1, true);
      else if (choice == 'w') tt.properties = setByteVal(tt.properties, 2, true);
      else if (choice == 'o') tt.properties = setByteVal(tt.properties, 3, true);

      MAP_TILETYPE_SET(newttlist[map->tilenumber], tt);
      checkfree(map->tiles);
      map->tiles = newttlist;
      map->tilenumber += 1;

      puts("");
    } else if (result == 'c') {
      // delete a tiletype
      printf("Input index of tiletype to delete: ");
      byte index = getnumber("error");
      
      // create a new list and put every tile in except for the one deleted
      newttlist = checkalloc(sizeof(tiletype) * (map->tilenumber - 1));
      byte currentNTTLIndex = 0;
      for (byte i = 0; i < map->tilenumber; i++) {
        if (i != index) {
          // MAP_TILETYPE_SET(newttlist[currentNTTLIndex], map->tiles[i]);
	  newttlist[currentNTTLIndex].properties = map->tiles[i].properties;
	  newttlist[currentNTTLIndex].color.red = map->tiles[i].color.red;
          newttlist[currentNTTLIndex].color.green = map->tiles[i].color.green;
	  newttlist[currentNTTLIndex].color.blue = map->tiles[i].color.blue;
	  currentNTTLIndex += 1;
	}
      }

      checkfree(map->tiles);
      map->tiles = newttlist;
      map->tilenumber -= 1;

      puts("");
    } else if (result == 'd') {
      printf("Input index of tiletype to change: ");
      byte index = getnumber("error");

      if (index > map->tilenumber) puts("Error: tile outside bounds of map");
      else am_modifytiletype(map, index - 1);

      puts("");
    } else if (result == 'e') {
      am_imagedata(img);
      
      puts("");
    } else if (result == 'q') {
      continueMAPLoop = false;
      puts("");
    }
  }
}
