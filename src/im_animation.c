// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see im_animation.h for more information

#include "f_datafile.h"
#include "i_err.h"
#include "i_file.h"
#include "i_salloc.h"
#include "i_string.h"
#include "im_animation.h"

#include <string.h>

#define MAX_BUFFER_SIZE 256

#define ANIM_LOAD_ERROR(msg) { anim->err = true; \
                               checkfree(anim->name); \
                               anim->name = strdup(msg); \
	                       return anim; }

animation *loadanim(FILE *stream, datafile *df) {
  // ascertain the stream is good
  if (!stream) error("Unable to load animation from null stream", false);

  char buffer[MAX_BUFFER_SIZE];

  animation *anim = checkalloc(sizeof(animation));
  anim->err = false;

  // ascertain the file is of the animation type
  checkfgets(buffer, MAX_BUFFER_SIZE, stream);
  if (strncmp(buffer, "LIT50", 5) != 0) ANIM_LOAD_ERROR("File is not a valid animation file");

  // read file id
  checkfgets(buffer, MAX_BUFFER_SIZE, stream);
  if (strlen(buffer) < 5) ANIM_LOAD_ERROR("Animation does not have valid datafile id");
  anim->name = strdup(buffer);

  // read version byte
  byte version;
  checkfread(&version, 1, 1, stream);

  // read newline
  checkfgets(buffer, 10, stream);
  
  // read in frame counter
  checkfread(&(anim->switchcntr), sizeof(unsigned short), 1, stream);

  // read to newline
  checkfgets(buffer, 10, stream);
  
  // set starttime to zero
  anim->starttime = 0;

  // read in deps
  checkfread(&(anim->depCount), 1, 1, stream);
  if (anim->depCount == 0) ANIM_LOAD_ERROR("Animation cannot have zero dependencies");

  anim->dependencies = checkalloc(sizeof(char *) * anim->depCount);
  anim->frames = checkalloc(sizeof(image_t *) * anim->depCount);

  datachunk *dc;

  for (byte i = 0; i < anim->depCount; i++) {
    checkfgets(buffer, MAX_BUFFER_SIZE, stream);
    if (strlen(buffer) < 5) ANIM_LOAD_ERROR("Animation dep does not have valid id");
    anim->dependencies[i] = strdup(buffer);

    // read in frame from dependency
    dc = loadnamedchunk(df, buffer, stream);
    if (!dc) ANIM_LOAD_ERROR("Unable to find animation dependency");
    if (dc->datatype != 48) ANIM_LOAD_ERROR("Animation dep is not a valid image");
    anim->frames[i] = dc->data.img;
  }

  return anim;
}

void saveanim(animation *anim, FILE *stream) {
  if (!anim) error("Unable to save null animation", false);
  if (anim->err) error("Unable to save animation with extant error", false);

  // write magic bytes + id
  fprintf(stream, "LIT50\n%s\n", anim->name);

  // write version
  byte version = CURRENT_ANIM_VERSION;
  checkfwrite(&version, 1, 1, stream);

  // write newline
  fputs("\n", stream);

  // write frame counter
  checkfwrite(&(anim->switchcntr), 1, 1, stream);
  fputs("\n", stream);

  // write out deps
  checkfwrite(&(anim->depCount), 1, 1, stream);
  fputs("\n", stream);

  for (byte i = 0; i < anim->depCount; i++) {
    fprintf(stream, "%s\n", anim->dependencies[i]);	  
  }
}

// delete an image from memory
void deleteanim(animation *anim) {
  if (anim) {
    if (anim->dependencies) {
      for (byte i = 0; i < anim->depCount; i++) checkfree(anim->dependencies[i]);
      checkfree(anim->dependencies);
    }

    checkfree(anim->name);
    if (anim->frames) {
      for (byte i = 0; i < anim->depCount; i++) deleteimg(anim->frames[i]);
      checkfree(anim->frames);
    }
    checkfree(anim);
  }
}

// run a tick on an animation
void animtick(animation *anim) {
  // check the starttime variable. if it is zero, we need to start counting time
  if (anim->starttime == 0) {
    anim->starttime = time(NULL);
    anim->currentframe = 0;
  } else {
    time_t currenttime = time(NULL);
    if (currenttime - anim->starttime > anim->switchcntr) {
      advanceframe(anim);
      anim->starttime = currenttime;
    }
  }
}

// advance the animation frame
void advanceframe(animation *anim) {
  anim->currentframe += 1;
  if (anim->currentframe >= anim->depCount) anim->currentframe = 0;
}

// get the current frame of the animation
image_t *getframe(animation *anim) {
  return anim->frames[anim->currentframe];
}
