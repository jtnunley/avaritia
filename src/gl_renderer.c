// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see gl_renderer.h for more information

#include "f_image.h"
#include "gl_renderer.h"
#include "gl_screen.h"
#include "i_byte.h"
#include "i_err.h"
#include "i_salloc.h"

#define WH_TO_INDEX(x,y,width) (y * width) + x
#define MIN(a, b) (a > b ? b : a)

// draw a pixeldata array
void drawpixeldata(glbuffer *buffer, byte *data, byte width, byte height, unsigned short x, unsigned short y, color_t *pallete, unsigned short pixXScale, unsigned short pixYScale, byte startingX, byte startingY, byte endingX, byte endingY) {
  unsigned int i, j, k, l;
  unsigned short index;
  color_t clr;

  // make sure all the variables are correctly assigned
  if (!data) error("Attempted to draw null data", true);
  if (width == 0 || height == 0) return;
  if (pixXScale == 0 || pixYScale == 0) return;
  if (startingY >= endingY || startingX >= endingX) return;
  if (endingY > height || endingX > width) return;

 //  byte i21 = img->height;

  // drawing loop
  byte photoX = startingX;
  byte photoY = startingY;
  //printf("Rendering map from {%d, %d} to {%d, %d}\n", startingX, startingY, endingX, endingY);
  for (j = y; j < y + (height * pixYScale); j += pixYScale) {
    for (i = x; i < x + (width * pixXScale); i += pixXScale) {
      // get data for the desired tile
      index = (photoY * width) + photoX;

      byte palleteIndex = data[index] - 1;
      SET_COLOR(clr, pallete[palleteIndex]);
      // printf("Pallete index is %d, color at index is {%d, %d, %d}\n",palleteIndex + 1,clr.red,clr.green,clr.blue);
      // printf("Drawing pixel at [%d, %d] (index %d) with color {%d, %d, %d}\n",i,j,index,clr.red,clr.green,clr.blue);
      if (!(IS_COLOR_ALPHA(clr))) {
	// draw each pixel, scaled up
	for (l = j; l < j + pixYScale; l++)
          for (k = i; k < i + pixXScale; k++) {
         
            modifyglob(buffer, k, l, clr);
	  }
      }

      photoX++;
      if (photoX > endingX - 1) break; // draws an extra row without - 1, for some reason
    }
    photoX = 0;
    photoY++;
    if (photoY > endingY - 1) break;
  }
}

void drawimageps(glbuffer *buffer, image_t *img, unsigned short x, unsigned short y, color_t *pallete, unsigned short pixXScale, unsigned short pixYScale) {
  if (!img) error("Attempted to draw null image", false);

  /* for (byte i = 0; i < img->palletelen; i++) {
    printf("Color #%d is {%d, %d, %d}\n", i + 1, img->palletedata[i].red, img->palletedata[i].green, img->palletedata[i].blue);
  }*/ 

  drawpixeldata(buffer, img->pixeldata, img->width, img->height, x, y, pallete, pixXScale, pixYScale, 0, 0, img->width, img->height);
}

void drawimagep(glbuffer *buffer, image_t *img, unsigned short x, unsigned short y, color_t *pallete) {
  drawimageps(buffer, img, x, y, pallete, 1, 1); 
}

void drawimages(glbuffer *buffer, image_t *img, unsigned short x, unsigned short y, unsigned short pixXScale, unsigned short pixYScale) {
  drawimageps(buffer, img, x, y, img->palletedata, pixXScale, pixYScale);
}

void drawimage(glbuffer *buffer, image_t *img, unsigned short x, unsigned short y) {
  drawimageps(buffer, img, x, y, img->palletedata, 1, 1);
}

void drawmap(glbuffer *buffer, map_t *map, unsigned short cornerX, unsigned short cornerY) {
  color_t color = {255, 0, 0xd8};
  byte tile;
  unsigned short i, j;
  byte k, l;
  unsigned short xIndex, yIndex;

  // get the oob color
  for (k = 0; k < map->tilenumber; k++)
    if (evalByteAt(map->tiles[k].properties, 3)) {
      SET_COLOR(color, map->tiles[k].color);
      break;
    }

  // printf("OOB color is {%d, %d, %d}\n", color.red,color.green,color.blue);

  // just set all tiles to the OOB color in case the map is smaller than the screen
  clearscreen(buffer, color); 

  // generate a color pallete
  color_t *pallete = checkalloc(sizeof(color_t) * map->tilenumber);
  for (byte i = 0; i < map->tilenumber; i++) {
    SET_COLOR(pallete[i], map->tiles[i].color);
  }

  // render the internal image 
  short endingX = MIN(map->width, cornerX + TILES_PER_SCREEN_WIDTH);
  short endingY = MIN(map->height, cornerY + TILES_PER_SCREEN_WIDTH);
  drawpixeldata(buffer, map->pixeldata, map->width, map->height, 0, 0, pallete, GLOBS_PER_TILE_WIDTH, GLOBS_PER_TILE_HEIGHT, cornerX, cornerY, endingX, endingY); 
}
