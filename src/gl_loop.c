// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

#include "g_gamestate.h"
#include "gl_draw.h"
#include "gl_loop.h"
#include "gl_screen.h"
#include "i_args.h"
#include "i_err.h"
#include "i_salloc.h"
#include "i_string.h"
#include "i_types.h"

// gl_screen variables, et al
unsigned short globsperscreenW;
unsigned short globsperscreenH;
// color_t **glscreen;

void initglscreen() {
  globsperscreenW = GLOBS_PER_TILE_WIDTH * TILES_PER_SCREEN_WIDTH;
  globsperscreenH = GLOBS_PER_TILE_HEIGHT * TILES_PER_SCREEN_HEIGHT;

  // make sure the total size isn't over USHRT_MAX
  int totalsize = globsperscreenW * globsperscreenH;
  if (totalsize > USHRT_MAX) error("Total size of glscreen is greater than the maximum USHRT value",false);
 
  /*globsperscreenW = 60;
  globsperscreenH = 60;*/

  // initccbuffer();
}

#include "g_tick.h"
#include "gl_buffer.h"
#include "gl_shaders.h"

// opengl headers
#include <stdio.h>
#include <stdlib.h>

#define GLEW_STATIC

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#define CHECK_ERR(s) while ((e = glGetError())) fprintf(stderr, "OpenGL Buffering Error: %d\n",e);

int beginGLLoop() {
  gamestate gt;
  gt.phase = 0;
  glbuffer gb;
  // gb.globcount = 0;
 
  // initialize gamestate
  int arg;
  if ((arg = argcheck("-d")) || ((arg = argcheck("--datafile")))) {
    gt.filename = strdup(argv_[arg + 1]);
  } else gt.filename = strdup("avaritia.adata");

  // before anything else, initialize the game
  initgame(&gt, "./letters.adata");
  puts("Game variables initialized");

  // firstly, create the window
  puts("Creating window...");

  GLenum e;

  printf("GL_INVALID_OPERATION is %d\n",GL_INVALID_OPERATION);
  printf("GL_INVALID_VALUE is %d\n",GL_INVALID_VALUE);
  printf("GL_INVALID_ENUM is %d\n",GL_INVALID_ENUM);
  
  // initialize GLFW
  glewExperimental = true;
  if (!glfwInit()) error("Failed to initialize GLFW",true);
  while (glGetError()); 
  
  // set window hints
  glfwWindowHint(GLFW_SAMPLES, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

  // actually create the window
  GLFWwindow *window;
  window = glfwCreateWindow(DEFAULT_SCREEN_WIDTH_PX,DEFAULT_SCREEN_HEIGHT_PX,"Avaritia",NULL,NULL);
  if (window == NULL) error("Unable to initialize window",true);
  glfwMakeContextCurrent(window);
  glewExperimental = true;
  if (glewInit() != GLEW_OK) error("Failed to initialize GLEW",true);
  CHECK_ERR("Window creation");

  puts("Windows created, initialzing screen variables...");

  // initialize screen variables
  initglscreen();

  puts("Screen variables initialized, generating vertex arrays...");

  // initialize a vertex array
  GLuint vertexarray;
  glGenVertexArrays(1, &vertexarray);
  glBindVertexArray(vertexarray);
  CHECK_ERR("Vertex array generation");

  puts("Vertex array generated, initializing buffers...");
  // generate vertex/index diagram
  GLuint vertexbuffer = initvertexdiagram();
  GLuint colorbuffer = initcolordiagram();
  GLuint texturebuffer = inittexturediagram();
  GLuint *indexbuffer = initindexdiagram();
  puts("Buffers have been loaded, loading shaders...");

  // compile shaders
  GLuint program = loadshaders("./src/shaders/vertex.glsl", "./src/shaders/fragment.glsl");
  CHECK_ERR("Shader compilation");

  puts("Creating common screen texture...");
  gb.buffer = createcolortexture();
  // printf("gb.buffer

  initdraw();

  puts("All setup has initialized successfully. Launching Avaritia...");

  // begin main loop
  glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
  bool operateLonger = true;
  // GLfloat *buffer;
  do {
    glClear(GL_COLOR_BUFFER_BIT);

    // before we run a game tick, set the entire screen to white
    /*glBindBuffer(GL_ARRAY_BUFFER,colorbuffer);
    buffer = glMapBuffer(GL_ARRAY_BUFFER,GL_WRITE_ONLY);
    for (unsigned int i = 0; i < globsperscreenW * globsperscreenH * 12; i++) {
      buffer[i] = 0.0f; // clear color
    }
    glUnmapBuffer(GL_ARRAY_BUFFER);*/

    // run a game tick
    operateLonger = gametick(&gt, &gb); 

    glUseProgram(program);
    drawscreen(window, program, vertexbuffer, colorbuffer, texturebuffer, gb.buffer, indexbuffer);

    glfwSwapBuffers(window);
    glfwPollEvents();

    CHECK_ERR("Event polling");

    operateLonger = operateLonger && (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS && glfwWindowShouldClose(window) == 0);
  } while (operateLonger);

  // delete some buffers
  glDeleteBuffers(1,&vertexbuffer);
  glDeleteBuffers(1,&colorbuffer);
  glDeleteBuffers(1, &texturebuffer);
  CHECK_ERR("Memory management");
  free(indexbuffer); 

  // return
  return 0;
}
