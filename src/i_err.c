// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see i_err.h for more info and tips on memory usage

#include "i_err.h"

#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <GLFW/glfw3.h>

void error(const char *msg, bool sys) {
  if (sys) fprintf(stderr, "Error: %s; %s\n",msg,strerror(errno));
  else fprintf(stderr, "Error: %s\n",msg);

  // glfwTerminate(); // TODO: maybe make condition for this?
  exit(1);
}
