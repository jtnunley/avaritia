// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see i_args.h for more information

// self header
#include "i_args.h"

// std lib headers
#include <string.h>

// define extern variables defined in i_args.h
int argc_;
char **argv_;

// this function looks for a given argument in argv_
int argcheck(const char *input) {
  for (int i = 0; i < argc_; i++) {
    if (!strcmp(input,argv_[i]))
      return i;
  }
  return 0;
}
