// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see am_interactive.h for more information

#include "am_interactive.h"
#include "i_file.h"
#include "i_string.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

#define MAX_BUFFER_SIZE 256

bool yesno(const char *prompt) {
  char buffer[MAX_BUFFER_SIZE];

  printf("%s  (Y/n): ",prompt);
  while (true) {
    fgets(buffer,MAX_BUFFER_SIZE,stdin);
    char firstch = tolower(buffer[0]);
    if (firstch == 'y') return true;
    else if (firstch == 'n') return false;
    else {
      printf("Invalid input, please enter yes (Y) or no (N): ");
    }
  }
}

// interactive menu
char createmenu(char options[], char *descriptions[], byte optNum) {
  // list options
  for (byte i = 0; i < optNum; i++) {
    printf("%c). %s\n",options[i],descriptions[i]);
  }
  
  // get answer
  printf("\nPlease enter an option: ");

  char answer;
  char buffer[10];
  while (true) {
    fgets(buffer,10,stdin);
    answer = tolower(buffer[0]);
    for (byte i = 0; i < optNum; i++) {
      if (answer == options[i]) return answer;
    }
    printf("Invalid input, enter a valid option: ");
  }
  return answer; // unreachable code
}

void readcolor(byte *r, byte *g, byte *b) {
  printf("Input color R, G, and B: ");
  while (true) {
    if (fscanf(stdin,"%hhd %hhd %hhd",r,g,b) != 3) {
      printf("Please input three numbers from 0 to 255 to represent a color: ");
    } else break;
  }
}

// get a number
byte getnumber(const char *prompt) {
  char buffer[MAX_BUFFER_SIZE];
  fputs(prompt,stdin);
  while (true) {
    checkfgets(buffer,MAX_BUFFER_SIZE,stdin);
   
    if (!(isstrnumber(buffer))) {
      printf("Please input a valid number: ");
      continue;
    }
    break;
  }
  return (byte)(atoi(buffer));
}
