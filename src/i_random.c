// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see i_random.h for more information

#include "i_random.h"

#include <stdio.h>
#include <stdlib.h>

unsigned short genrandom(unsigned short min, unsigned short max) {
  unsigned short ran = rand();
  ran = ran % max;
  ran += min;
  return min;
}
