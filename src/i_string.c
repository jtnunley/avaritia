// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see i_string.h for more information

#include "c_unistd.h"
#include "i_string.h"
#include "i_salloc.h"
#include "i_err.h"

#ifdef __STDC_ALLOC_LIB__
#define __STDC_WANT_LIB_EXT2__ 1
#else
#define _POSIX_C_SOURCE 200809L
#endif

#include <assert.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

void removenewline(char *str) {
  // get string length
  size_t length = strlen(str);

  // determine if the last character is a newline
  if (str[length - 1] == '\n') 
    str[length - 1] = '\0'; // replace the last character with a null terminator
}

bool isstrnumber(char *str) {
  for (byte i = 0; i < strlen(str); i++) {
    if (!isdigit(str[i])) return false;
  }
  return true;
}

// "quick and dirty" solution but it works
char **splitstring(char *str, const char delim, unsigned short *count) {
  // find the # of occurences of the delimiter
  unsigned short occurrences = 0;
  for (unsigned short i = 0; i < strlen(str); i++) {
    if (str[i] == delim) occurrences++;
  }

  //printf("%d occurrences of %c found in \"%s\"\n",occurrences,delim,str);

  // create memory
  char **result = checkalloc(sizeof(char *) * (occurrences + 1));
  char *buffer = checkalloc(sizeof(char) * (strlen(str) + 1));
  unsigned short bufferlen = 0;
  unsigned short resultIndex = 0;

  for (unsigned short i = 0; i < strlen(str); i++) {
    if (str[i] == delim) {
      //printf("Found delimiter at index %d\n",i);

      if (bufferlen == 0) { 
	//printf("Duplication delimiter, skipping...\n");
        continue;
      }
      buffer[bufferlen] = '\0';
      // printf("Buffer \"%s\" put into results at index %d",buffer,resultIndex + 1);
      result[resultIndex++] = buffer;
      buffer = checkalloc(sizeof(char) * (strlen(str) + 1));
      bufferlen = 0;
    } else {
      // printf("No delimiter at index %d\n",i);
      buffer[bufferlen] = str[i];
      bufferlen++;
    }
  }

  if (bufferlen > 0) {
    buffer[bufferlen] = '\0';
    // printf("Final buffer identified as \"%s\"\n",buffer);
    result[resultIndex++] = buffer;
  }
  else free(buffer);
  (*count) = resultIndex;

  return result;
}

// duplicate a string
char *strdup(const char *str) {
  if (!str) error("Attemtping to duplicate null string", false);

  char *result = checkalloc(sizeof(char) * (strlen(str) + 1));
  strcpy(result, str);

  if (strncmp(str, result, strlen(str)) != 0) error("String copy operation failed",true);

  return result;
}
