// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see m_map.h for more information

// #include "f_datachunk.h"
#include "f_datafile.h"
#include "m_map.h"
#include "i_err.h"
#include "i_file.h"
#include "i_salloc.h"
#include "i_string.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_BUFFER_SIZE 256
#define MIN(a,b) (a < b ? a : b)

// load a map from a stream
map_t *loadmap(FILE *stream, datafile *df) {
  // make room in memory for the map
  //FILE *stream = getreadstream(ds);
  map_t *mp = checkalloc(sizeof(map_t));

  // signifies a loading error in the name
  #define MAP_LOAD_ERROR(msg) { mp->err = true; \
	                        if (mp->name) free(mp->name); \
	                        mp->name = strdup(msg); return mp; }

  // read the first line from the stream
  char buffer[MAX_BUFFER_SIZE];
  checkfgets(buffer,MAX_BUFFER_SIZE,stream);
  
  if (strcmp(buffer,"LIT49") != 0) MAP_LOAD_ERROR("File is not a valid map file");

  // read the map's id from the stream
  checkfgets(buffer,MAX_BUFFER_SIZE,stream);
  
  if (strlen(buffer) < 5) MAP_LOAD_ERROR("File does not contain a valid map id");
  mp->name = strdup(buffer);

  // read version and length bytes
  checkfread(&(mp->version), 1, 1,stream);
  // checkfread(&(mp->len),sizeof(unsigned int),1,stream);
  checkfgets(buffer, 2, stream);

  // read # of tiletypes
  checkfread(&(mp->tilenumber), 1, 1, stream);
  checkfgets(buffer, 2, stream);

  mp->tiles = checkalloc(sizeof(tiletype) * mp->tilenumber);
 
  for (byte i = 0; i < mp->tilenumber; i++) {
    mp->tiles[i].properties = 0;
  }

  // read tiletypes, each are four bytes
  byte tiletypebuffer[4];
  for (byte i = 0; i < mp->tilenumber; i++) {
    checkfread(tiletypebuffer, 1, 4, stream);
    mp->tiles[i].properties = tiletypebuffer[0];
    mp->tiles[i].color.red = tiletypebuffer[1];
    mp->tiles[i].color.green = tiletypebuffer[2];
    mp->tiles[i].color.blue = tiletypebuffer[3];
  }

  // read newline
  checkfgets(buffer, 2, stream);

  // read LIT03
  checkfgets(buffer,MAX_BUFFER_SIZE,stream);
  if (strcmp(buffer,"LIT03") != 0) MAP_LOAD_ERROR("File does not have dependencies");

  // read dependencies
  readbytes(tiletypebuffer,stream,1);
  mp->dependencynumber = tiletypebuffer[0];
  if (mp->dependencynumber == 0) MAP_LOAD_ERROR("Map does not have the required number of dependencies");
  mp->dependencies = checkalloc(sizeof(char *) * mp->dependencynumber);
  for (byte i = 0; i < mp->dependencynumber; i++) {
    checkfgets(buffer,MAX_BUFFER_SIZE,stream);
    mp->dependencies[i] = strdup(buffer);
  }

  // 1st dependency should be an image
  datachunk *chunk = loadnamedchunk(df,mp->dependencies[0],stream);
  if (chunk->datatype != 48) { 
    unloadchunk(chunk);
    MAP_LOAD_ERROR("First map dependency is not an image");
  }

  image_t *img = chunk->data.img;
  mp->width = img->width;
  mp->height = img->height;
  mp->pixeldata = img->pixeldata;

  // return the map
  return mp;
}

// save the map
void savemap(FILE *stream, map_t *map) {
  if (!map) error("Map must exist", true);

  // write LIT49\n
  fprintf(stream, "LIT49\n");

  // write the map's name
  fprintf(stream, "%s\n", map->name); 

  // write version/len byte
  byte mapversion = CURRENT_MAP_VERSION;
  fwrite(&(mapversion), 1, 1, stream);
  // fwrite(&(map->len),sizeof(unsigned int),1,stream);
  fprintf(stream,"\n");

  // write tile number followed by tile types 
  byte tiletypebuffer[4];
  fwrite(&(map->tilenumber), 1, 1, stream);
  fprintf(stream,"\n");
  tiletype tt;
  for (byte i = 0; i < map->tilenumber; i++) {
    tt = map->tiles[i];
    tiletypebuffer[0] = tt.properties;
    tiletypebuffer[1] = tt.color.red;
    tiletypebuffer[2] = tt.color.green;
    tiletypebuffer[3] = tt.color.blue;
    checkfwrite(tiletypebuffer, 1, 4, stream);
  }

  fprintf(stream, "\n");

  // write LIT03 followed by depcount
  fprintf(stream,"LIT03\n");
  fwrite(&(map->dependencynumber), 1, 1, stream);

  //printf("Map depcount is %d\n",map->dependencynumber);
  // write deps
  for (byte i = 0; i < map->dependencynumber; i++) {
    fprintf(stream,"%s\n",map->dependencies[i]);
    //printf("Map dep is %s\n",map->deps[i]);
  }
}

// delete a map from memory
void deletemap(map_t *map) {
  if (map) {
    // delete deps pointers
    if (map->dependencies) {
      for (byte i = 0; i < map->dependencynumber; i++) {
        free(map->dependencies[i]);
      }
      free(map->dependencies);
    }

    free(map->name);
    free(map);
  }
}
