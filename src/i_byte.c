// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see i_byte.h for more information

#include "i_byte.h"
#include "i_err.h"

// get the upper half of a byte (first four bits)
byte upperhalf(byte b) {
  return (b >> 4) & 15;
}

// get the lower half of a byte (last four bits)
byte lowerhalf(byte b) {
  return b & 15;
}

// combine two byte halves into one byte
byte combinehalves(byte upper, byte lower) {
  byte b = 0;
  lower = lower & 15;
  upper = (upper << 4) & (15 << 4);
  b = b | lower;
  b = b | upper;
  return b;
}

// evalulate a byte at a certain index
bool evalByteAt(byte b, byte index) {
    if (index > 7) error("evalByteAt() passed invalid index variable", false);

    byte mask = 0x01 << index;
    return (b & mask) != 0;
}

// set a byte's value at a certain index
byte setByteVal(byte b, byte index, bool value) {
    if (index > 7) error("setByteVal() passed invalid index variable", false);

    byte mask = 0x01 << index;
    if (value)
         return b | mask;
    else
         return b & ~(mask);
}
