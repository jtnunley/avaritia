// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see gl_draw.h for more information

#include "i_err.h"
#include "i_salloc.h"
#include "i_types.h"

#include "gl_draw.h"
#include "gl_screen.h"

#include <stdio.h>
#include <stdlib.h>

#define CHECK_ERR(s) while ((e = glGetError())) fprintf(stderr,"OpenGL Error in %s: %d\n",s,e);

#define TO_GL_CLR(val) (float)(val) / 255

// initialize collection of vertices used to draw stuff
GLuint initvertexdiagram() {
  // 4 vertices
  static const GLfloat screenvertices[] = {
    1.0f, 1.0f, 0.0f,
    1.0f, -1.0f, 0.0f,
    -1.0f, -1.0f, 0.0f,
    -1.0f, 1.0f, 0.0f,
  };

  GLenum e;

  // setup opengl-style buffer
  GLuint buffer;
  glGenBuffers(1,&buffer);
  glBindBuffer(GL_ARRAY_BUFFER, buffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 4 * 3, screenvertices, GL_STATIC_DRAW);
  CHECK_ERR("Vertex buffering");

  // free(verticies);

  // return the pointer
  return buffer;
}

// initialize a buffer of colors
GLuint initcolordiagram() {
  size_t size = sizeof(GLfloat) * 4 * 3;
  printf("Color diagram size is %ld\n",size);
  GLfloat *colors = checkalloc(size);

  #define CLIP_TO_1P0(val) while (val > 1.0f) val -= 1.0f;

  GLenum e;

  // default color is pink
  color_t pink = {0xff, 0x69, 0xb4};
  for (byte i = 0; i < 4; i++) {
    byte j = i * 3;
    colors[j] = TO_GL_CLR(pink.red);
    colors[j + 1] = TO_GL_CLR(pink.green);
    colors[j + 2] = TO_GL_CLR(pink.blue);
  }

  // setup buffer
  GLuint buffer;
  glGenBuffers(1,&buffer);
  glBindBuffer(GL_ARRAY_BUFFER,buffer);
  glBufferData(GL_ARRAY_BUFFER,size,colors,GL_STATIC_DRAW);
  CHECK_ERR("Color buffering");

  checkfree(colors);

  return buffer;
}

// initialize a buffer containing texture coordinates
GLuint inittexturediagram() {
  static const float texcoords[] = {
    1.0f, 1.0f,
    1.0f, 0.0f,
    0.0f, 0.0f,
    0.0f, 1.0f
  };

  // set up buffer
  GLuint buffer;
  GLenum e;
  glGenBuffers(1, &buffer);
  glBindBuffer(GL_ARRAY_BUFFER, buffer);
  glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 4 * 2, texcoords, GL_STATIC_DRAW);
  CHECK_ERR("Tex Coordinate Buffering");

  return buffer;
}

// initialize an index buffer
GLuint *initindexdiagram() {
  unsigned int squareCount = 1;
  size_t size = sizeof(GLuint) * 6 * squareCount;
  GLuint *indicies = checkalloc(size);
  unsigned int currentIndex = 0;
  
  indicies[currentIndex] = 0;
  indicies[currentIndex + 1] = 1;
  indicies[currentIndex + 2] = 2;
  indicies[currentIndex + 3] = 0;
  indicies[currentIndex + 4] = 2;
  indicies[currentIndex + 5] = 3;

  // print this
  /*printf("Indices: {");
  for (unsigned short k = 0; k < squareCount * 6; k++) {
    printf("%d, ",indicies[k]);
  }
  puts("}");*/

  // return
  return indicies;
}

// create a 2d texture for storing color data
GLuint createcolortexture() {
  // generate and bind texture
  GLuint texture;
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

  // set up image
  // note: final parameter is zero, so all values within should be zero until set by rendering programs
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, globsperscreenW, globsperscreenH, 0, GL_RGB, GL_UNSIGNED_BYTE, (void *)0);

  // return the texture
  return texture;
}

void initdraw() {
  GLenum e;
  glClearColor(0.0f, 0.4f, 0.4f, 0.0f);
  CHECK_ERR("Drawing initialization");
}

// change a singular color in a color buffer
void changecolor(GLuint texture, unsigned short x, unsigned short y, color_t clr) {
  // printf("Changing glob at [%d, %d] to {%d, %d, %d}\n", x, y, clr.red, clr.green, clr.blue);

  GLbyte data[3];
  data[0] = clr.red;
  data[1] = clr.green;
  data[2] = clr.blue;
  // data[3] = 255; // no transparency allowed

  glBindTexture(GL_TEXTURE_2D, texture);
  glTexSubImage2D(GL_TEXTURE_2D, 0, x, y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, data);
}

void drawscreen(GLFWwindow *window, GLuint program, GLuint vertexbuffer, GLuint colorbuffer, GLuint texturebuffer, GLuint texture, GLuint *indicies) {
  GLenum e;

  // get window width and height
  int width, height;
  glfwGetWindowSize(window, &width, &height);
 
  // processcolorchangequeue(colordiagram);
  // 1st attribute buffer: vertices
  glEnableVertexAttribArray(0);
  glBindBuffer(GL_ARRAY_BUFFER,vertexbuffer);
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
  CHECK_ERR("Buffering vertices");

  // 2nd attribute buffer: colors
  glEnableVertexAttribArray(1);
  glBindBuffer(GL_ARRAY_BUFFER,colorbuffer); 
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void *)0);
  CHECK_ERR("Buffering colors");

  // 3rd attribute buffer: texture coordinates
  glEnableVertexAttribArray(2);
  glBindBuffer(GL_ARRAY_BUFFER, texturebuffer);
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, (void *)0);
  CHECK_ERR("Buffering texture coordinates");

  // set uniforms
  GLuint widthUniform = glGetUniformLocation(program,"wwidth");
  GLuint heightUniform = glGetUniformLocation(program,"wheight");
  glProgramUniform1i(program,widthUniform,width);
  glProgramUniform1i(program,heightUniform,height);

  GLuint origWUniform = glGetUniformLocation(program,"origwidth");
  GLuint origHUniform = glGetUniformLocation(program,"origheight");
  glProgramUniform1i(program,origWUniform,DEFAULT_SCREEN_WIDTH_PX);
  glProgramUniform1i(program,origHUniform,DEFAULT_SCREEN_HEIGHT_PX);

  GLuint gpsWUniform = glGetUniformLocation(program,"globsperscreenW");
  GLuint gpsHUniform = glGetUniformLocation(program,"globsperscreenH");
  glProgramUniform1i(program, gpsWUniform, globsperscreenW);
  glProgramUniform1i(program, gpsHUniform, globsperscreenH);

  CHECK_ERR("Uniform setting");

  // draw squares
  glDrawElements(GL_TRIANGLE_STRIP, 6, GL_UNSIGNED_INT, indicies);
  glDisableVertexAttribArray(0);

  // print errors
  while ((e = glGetError())) fprintf(stderr, "OpenGL Render Error %d\n",e);
}
