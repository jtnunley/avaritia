// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see f_image.h for more information

#include "f_image.h"
#include "i_file.h"
#include "i_byte.h"
#include "i_err.h"
#include "i_salloc.h"
#include "i_string.h"
#include "i_types.h"

#include <stdlib.h>
#include <string.h>

// testing
#include "am_dfops.h"

#define MAX_BUFFER_SIZE 256
#define IMAGE_LOAD_ERROR(msg) { img->err = true; \
                         img->name = checkalloc(strlen(msg) + 1); \
	                 strcpy(img->name,msg); \
                         return img; }

#define READ_TO_NEWLINE checkfgets(charbuffer,MAX_BUFFER_SIZE,stream); 

#define WH_TO_INDEX(x,y,width) (y * width) + x

// load image from stream
image_t *loadimg(FILE *stream) {
//  puts("Beginning image read");

  // allocate memory space for image
  image_t *img = checkalloc(sizeof(image_t));

  if (!stream) error("Stream object is nil",false); 
  

  // firstly, ensure that the first bytes of the file are "LIT48\n" (magic bytes for images)
  char charbuffer[MAX_BUFFER_SIZE];
  byte bytebuffer[MAX_BUFFER_SIZE];  

  checkfgets(charbuffer,MAX_BUFFER_SIZE,stream); // read newline-terminated string

  if (strcmp(charbuffer,"LIT48") != 0) IMAGE_LOAD_ERROR("File is not a AMIF image\n");

//  puts("reading id");

  // secondly, read image id from the file
  checkfgets(charbuffer,MAX_BUFFER_SIZE,stream);
  if (strlen(charbuffer) < 0) IMAGE_LOAD_ERROR("Image does not have a valid name\n"); 
  
  img->name = strdup(charbuffer); 
 
  // next, read past version and image bytes
  // these may be used later
  checkfread(&(img->version),1,1,stream);
  if (img->version > CURRENT_IMAGE_VERSION) IMAGE_LOAD_ERROR("Image does not have a valid version byte");
  checkfgets(charbuffer,MAX_BUFFER_SIZE,stream);

//  puts("Reading property bytes");

  // thirdly, read three bytes from the file: width, height, and length of color pallette
  readbytes(bytebuffer,stream,3);

  img->width = bytebuffer[0];
  img->height = bytebuffer[1];
  img->palletelen = bytebuffer[2];

  // do some bounds checking
  if (img->palletelen > 16) IMAGE_LOAD_ERROR("Image has too high of a pallete length\n");

  // allocate pallete data
  img->palletedata = checkalloc(sizeof(color_t) * img->palletelen);

//  puts("Reading colors");

  // fourthly, read color data from the file. color is three bytes, rgb order
  color_t clr;
  for (byte i = 0; i < img->palletelen; i++) {
    readbytes(bytebuffer,stream,3);
    clr.red = bytebuffer[0];
    clr.green = bytebuffer[1];
    clr.blue = bytebuffer[2];
    img->palletedata[i] = clr;
  }

  // there should be a newline after all of this; read to newline
  READ_TO_NEWLINE;

  // fifthly, using width and height information, read pixel data
  
  // allocate pixel data
  img->pixeldata = checkalloc(sizeof(byte) * img->width * img->height);

  byte upperByte,lowerByte;
  unsigned short pixeldataIndex;

  for (byte i = 0; i < img->height; i++) { // iterate over rows
     // image data is stored in two indices per byte, so total # of bytes per row is width / 2 rounded up
     byte rowlen = (img->width + 1) / 2; // rounded up
     for (byte j = 0; j < rowlen; j++) {
       readbytes(bytebuffer,stream,1);

       // dissassemble bytes
       upperByte = upperhalf(bytebuffer[0]);
       lowerByte = lowerhalf(bytebuffer[0]);

       pixeldataIndex = WH_TO_INDEX(j*2,i,img->width);

       // printf("readimg: bytes are %d and %d, accessing index at %d\n",upperByte,lowerByte,pixeldataIndex);

       img->pixeldata[pixeldataIndex] = upperByte;

       if ((j + 1) * 2 <= img->width) { 
         img->pixeldata[pixeldataIndex + 1] = lowerByte;
       }
     }

     // read to a newline byte
     READ_TO_NEWLINE;
  }

  // return the end image
  return img;
}

// save an image to a file
void saveimg(image_t *img, FILE *stream) {
  if (!stream) error("Stream does not exist", true);
  // printf("Stream file number: %d\n", stream->_fileno);
  // puts("Beginning image save");

  // first, write "LIT48\n" to the file
  fprintf(stream,"LIT48\n"); 

  // next, write the file id to the file
  fprintf(stream,"%s\n",img->name); 

  // next, write image version and length
  checkfwrite(&(img->version),1,1,stream);
  //checkfwrite(&(img->len),sizeof(unsigned int),1,stream);
  fprintf(stream,"\n");

  // create buffer to load bytes into'
  byte *buffer = checkalloc(sizeof(byte) * MAX_BUFFER_SIZE);

  // load width height and pallette len into buffer
  SET_BUFFER_BYTES(buffer,img->width,img->height,img->palletelen,0,0);

  // write buffer
  writebytes(buffer,stream,3);

  // load colors into buffer and write them
  for (byte i = 0; i < img->palletelen; i++) {
    color_t clr = img->palletedata[i];
    SET_BUFFER_BYTES(buffer,clr.red,clr.green,clr.blue,0,0);
    writebytes(buffer,stream,3);
  }

  // write a '\n' char
  buffer[0] = '\n';
  writebytes(buffer,stream,1);

  // write rows
  for (byte i = 0; i < img->height; i++) {
    for (byte j = 0; j < img->width; j += 2) {
      unsigned short pixelindex = WH_TO_INDEX(j,i,img->width);
     
      byte lowerhalf = (pixelindex + 1 >= img->width * img->height) ? 0 : img->pixeldata[pixelindex + 1];
      buffer[0] = combinehalves(img->pixeldata[pixelindex],lowerhalf);
      // printf("Upper half is %x, lower half is %x, combined byte is %x\n", img->pixeldata[pixelindex], lowerhalf, buffer[0]);
      fwrite(buffer, 1, 1, stream);
    }
    buffer[0] = '\n';
    writebytes(buffer,stream,1);
  }

  // puts("ending image save");
  // printf("Stream file number at end of func: %d\n", stream->_fileno);
}

color_t getpixel(image_t *img, byte x, byte y) {
  byte index = img->pixeldata[WH_TO_INDEX(x,y,img->width)];
  return img->palletedata[index]; 
}

// free image data
void deleteimg(image_t *img) {
  if (img->palletedata) checkfree(img->palletedata);
  checkfree(img->pixeldata);
  checkfree(img);
}
