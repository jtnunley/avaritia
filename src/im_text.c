// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see im_text for more information

#include "f_datafile.h"
#include "f_image.h"
#include "gl_renderer.h"
#include "i_err.h"
#include "i_salloc.h"
#include "im_text.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

datafile *textstorage;

#define IS_CAPITAL_LETTER

void inittextrender(const char *textfile) {
  char *err = checkalloc(1);
  textstorage = loaddatafile(textfile,err);
  if (!textstorage) {
    const char *format = "Unable to find font; %s";
    char *errMsg = checkalloc(sizeof(char) * (strlen(format) - 1 + strlen(err)));
    sprintf(errMsg,format,err);
    error(errMsg,false);
  }
  checkfree(err);

  // check datafile
  /*FILE *stream = fopen(textstorage->filename,"rb");
  for (unsigned short i = 0; i < textstorage->datachunkslen; i++) {
    printf("Letter #%d: %s\n",i,textstorage->datachunks[i]->id);
    loadchunk(textstorage->datachunks[i],stream,textstorage);
  } 
  fclose(stream);*/
}

void rendertxt(glbuffer *buffer, const char *txt, unsigned short x, unsigned short y, color_t clr) {
  unsigned short len = strlen(txt);
  char ch;
  datachunk *dc;
  image_t *img;

  FILE *stream = fopen(textstorage->filename,"rb");
  const char *format = "img_letter_%c";
  char *datachunkname = checkalloc(40);
  
  const byte spaceBetweenLetters = 1;
  unsigned short currentOffset = x;
 
  bool foundDC;

  // generate pallete
  color_t *pallete = checkalloc(sizeof(color_t) * 2);
  pallete[0].red = clr.red;
  pallete[0].green = clr.green;
  pallete[0].blue = clr.blue;
  pallete[1].red = C_ALPHA_RED;
  pallete[1].green = C_ALPHA_GREEN;
  pallete[1].blue = C_ALPHA_BLUE;

  // take each char at a time
  for (unsigned short i = 0; i < len; i++) {
   foundDC = true;
   ch = tolower(txt[i]);

   if (isalpha(ch)) {
      //printf("Ealuating %c\n",ch);

      // setup letter
      sprintf(datachunkname,format,ch);
    } else if (isdigit(ch)) {
      sprintf(datachunkname,"img_number_%c",ch);
    } else if (ch == ' ') {
      strcpy(datachunkname,"img_punct_space");
    } else if (ch == '!') {
      strcpy(datachunkname,"img_punct_exclam");
    } else if (ch == '.') {
      strcpy(datachunkname,"img_punct_period"); 
    } else if (ch == ':') {
      strcpy(datachunkname,"img_punct_colon");
    } else foundDC = false;

    if (foundDC) {
      // printf("Loading %s\n",datachunkname);
      dc = loadnamedchunk(textstorage, datachunkname, stream);
      if (!dc) error("Font failed to load",true);
      //printf("Loaded datachunk\n");

      img = dc->data.img;
      if (!img) error("Font image casting returned null",true);
      //printf("Loaded image\n");


      // print to screen
      drawimageps(buffer,img,currentOffset,y,pallete,1,1);
      currentOffset += spaceBetweenLetters + img->width;
    }
  }

  fclose(stream);
  free(datachunkname);
}
