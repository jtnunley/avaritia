// this file is a part of Avaritia, made by John Nunley and licensed under GPL 3.0

// see am_main.h for more information

#include "am_dfops.h"
#include "am_interactive.h"
#include "am_main.h"
#include "f_datafile.h"
#include "f_imageconv.h"
#include "i_err.h"
#include "i_file.h"
#include "i_salloc.h"
#include "i_string.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define BUFFER_LIMIT 256

#define GET_DATAFILE_NAME printf("Input datafile name: "); \
                          while (true) { \
                            checkfgets(buffer,BUFFER_LIMIT,stdin); \
	                    if (strlen(buffer) < 5) { \
                              printf("Datafile name must be at least 5 characters in length: "); \
	                    } else break; \
                          } \
                            \
			  checkfree(df->name); \
                          df->name = strdup(buffer);

void beginassetmanager() {
  puts("Initializing Avaritia's internal AssetManager...\n");

  char buffer[BUFFER_LIMIT];
  
  // prompt for a file
  printf("Enter the name of the datafile you would like to read: ");
  checkfgets(buffer,256,stdin);

  // keep the filename around
  char *filename = checkalloc(sizeof(char) * (strlen(buffer) + 1));
  strcpy(filename,buffer);

  bool dfCreatedFromScratch = false;

  // printf("%s\n",buffer);

  datafile *df;
  char *err = strdup("noerr");

  if (!(fileexists(buffer))) {
    if (yesno("File does not exist, would you like to create a new datafile?")) {
      df = checkalloc(sizeof(datafile));
      df->chunknumber = 0;
      df->filename = strdup(buffer);
      df->version = CURRENT_DFILE_VERSION;
      df->datachunks = checkalloc(sizeof(datachunk) * MAX_DATACHUNKS);
      df->initialmap = strdup("(null)");

      // get name of df 
      df->name = checkalloc(3);
      GET_DATAFILE_NAME;
      dfCreatedFromScratch = true;

      df->currentpos = 9 + strlen(df->name);
    } else {
      puts("Nothing to do; exiting Avaritia AssetManager...");
      return;
    }
  } else {
    df = loaddatafile(buffer,err);
    if (strcmp("noerr",err) != 0) error(err,false);
   
    FILE *tempstream = fopen(df->filename,"rb");
    df->currentpos = getfilesize(tempstream);
    fclose(tempstream);
  } 

  checkfree(err);

  // main loop
  bool continueAMLoop = true,
       unsavedWork = dfCreatedFromScratch;

  // define options for menu
  char options[] = {'a','b','c','d','e','s','q'};
  char *descriptions[] = {"Change datafile name",
	                  "Change/Define initially loaded map",
	                  "Create new datachunk",
		          "Modify existing datachunk",
			  "Delete a datachunk",
			  "Save datafile",
			  "Quit AssetManager"};
  byte optNum = 7; // adjust as necessary

  // options for other menu
  char cm_options[] = {'a','b','c','q'};
  char *cm_descs[] = {"Create image",
	              "Import image from file",
	              "Create map",
	              "Go back"};
  byte cm_optnum = 4;
  while (continueAMLoop) {
    puts("");
    summarizedatafile(df);
    if (unsavedWork) puts("File contains unsaved work.");
    puts("");

    char answer = createmenu(options,descriptions,optNum);
    switch (answer) {
      case 'q':
        if (unsavedWork) {
          if (yesno("You have unsaved work. Would you like to save it?")) {
            savedatafile(df);
	    printf("Saved df to %s\n",filename);
	  }
	}
	continueAMLoop = false;
        break;
      case 'a':
        GET_DATAFILE_NAME;
	unsavedWork = true;
	break;
      case 'b':
	printf("Input initial map: ");
        while (true) {
          checkfgets(buffer, 256, stdin);
	  if (strlen(buffer) < 5) {
	    printf("Please input a valid id: ");
	    continue;
	  }
	  df->initialmap = strdup(buffer);
          break;
	}
	break;
      case 'c':
	answer = createmenu(cm_options,cm_descs,cm_optnum);
	if (answer == 'a')
	  am_createimg(df);
	if (answer == 'c')
	  am_createmap(df);
	if (answer == 'b') {
	  image_t *img;
          while (true) {
            printf("Enter filename of image: ");

            checkfgets(buffer,BUFFER_LIMIT,stdin);
	    img = loadimgexternal(buffer);
	    if (img->err) {
	      printf("Error loading image: %s\n",img->name);
	      free(img);
	    }
	    else break;
	  }

	  // get id for image
	  printf("Enter ID of image: ");
	  while (true) {
            checkfgets(buffer,BUFFER_LIMIT,stdin);
	    if (strlen(buffer) < 5) printf("Please input a valid ID: ");
	    else break;
	  }
	  img->name = strdup(buffer);

	  datachunk *imgdcext = checkalloc(sizeof(datachunk));
	  imgdcext->name = strdup(buffer);
	  imgdcext->status = 2;
	  imgdcext->pos = -1;
	  imgdcext->datatype = 48;
	  imgdcext->data.img = img;
	  df->datachunks[df->chunknumber] = imgdcext;
	  df->chunknumber += 1;
	}
	if (answer != 'q')
	  unsavedWork = true;
	break;
      case 'd':
	printf("Enter datachunk id to modify: ");
	while (true) {
          checkfgets(buffer,BUFFER_LIMIT,stdin);
	 
	  if (strlen(buffer) < 5) {
	    printf("Please enter a valid datachunk id: ");
	    continue;
	  }
	  
	  bool foundDc = false;
	  // check to make sure it's in there
	  for (unsigned short i = 0; i < df->chunknumber; i++) {
            if (strcmp(df->datachunks[i]->name,buffer) == 0) {
              if (df->datachunks[i]->datatype == 48) am_modifyimg(df,buffer);
	      else if (df->datachunks[i]->datatype == 49) am_modifymap(df,buffer);

	      unsavedWork = true;
	      foundDc = true;
	      break;
	    }
	  }

	  if (foundDc) break;

	  printf("Datachunk %s not found: ",buffer);
	}
	break;
      case 'e':
	printf("Enter datachunk id to delete: ");
	while (true) {
          checkfgets(buffer,BUFFER_LIMIT,stdin);
	  if (strlen(buffer) < 5) {
            printf("Please input a valid id: ");
	    continue;
	  }

	  // try to find a datachunk
	  bool foundDatachunk = false;
	  for (unsigned short i = 0; i < df->chunknumber; i++) {
             if (strcmp(df->datachunks[i]->name,buffer) == 0) {
               unloadnamedchunk(df,buffer); // unload to reduce complications
	       free(df->datachunks[i]);
	       df->datachunks[i] = 0;
	       foundDatachunk = true;

	       df->chunknumber -= 1;
	       if (i == df->chunknumber) break;

	       // re arrange datachunk list
	       for (unsigned short j = i; j < MAX_DATACHUNKS; j++) {
                 if (!(df->datachunks[j + 1])) break;
		 df->datachunks[j] = df->datachunks[j + 1];
	       }
	     }
	  }

	  if (foundDatachunk) break;

	  printf("Datachunk %s not found: ",buffer);
	}
	break;
      case 's':
	savedatafile(df);
	printf("Saved df to %s\n",filename);
	unsavedWork = false;
	break;
    }
  }

  for (unsigned int i = 0; i < df->chunknumber; i++) {
    if (!(df->datachunks[i])) continue;
    unloadchunk(df->datachunks[i]);
    free(df->datachunks[i]);
  }
  free(df);
  if (fileexists("tmp.txt")) checkremove("tmp.txt");

  puts("Exiting Avaritia AssetManager...");
}
